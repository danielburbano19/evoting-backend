-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-06-2020 a las 19:55:39
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `evoting`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grant_table`
--

CREATE TABLE `grant_table` (
  `id` int(11) NOT NULL,
  `grant_type` char(1) NOT NULL,
  `module` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `identification_type`
--

CREATE TABLE `identification_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `identification_type`
--

INSERT INTO `identification_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cédula de ciudadanía', '2020-06-21 13:27:06', '2020-06-21 13:27:14'),
(2, 'cédula de extranjeria', '2020-06-21 13:27:21', '2020-06-21 13:27:25'),
(3, 'Pasaporte', '2020-06-21 13:27:30', '2020-06-21 13:27:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `import_type`
--

CREATE TABLE `import_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `import_type`
--

INSERT INTO `import_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Importación de los votantes del sistema', '2020-06-21 16:29:41', '2020-06-21 17:55:50'),
(2, 'Importación de candidatos', '2020-06-21 16:22:56', '2020-06-21 16:22:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `module`
--

CREATE TABLE `module` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `role`
--

INSERT INTO `role` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'Rol usado para la administración del sistema e-voting', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Votante', 'Rol usado para los usuarios votantes en un proceso de elección', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_grant_table`
--

CREATE TABLE `role_grant_table` (
  `role_id` int(11) NOT NULL,
  `grant_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `info` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `password` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `info`, `role`, `user_name`, `password`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'dburbano', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 61, 2, '12345678', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '2020-06-22 12:31:28', '2020-06-22 12:31:28'),
(28, 62, 2, '87654321', 'e24df920078c3dd4e7e8d2442f00e5c9ab2a231bb3918d65cc50906e49ecaef4', '2020-06-22 12:31:28', '2020-06-22 12:31:28'),
(30, 10, 1, 'juanCandidato', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '2020-06-22 12:52:32', '2020-06-22 12:52:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_importer`
--

CREATE TABLE `user_importer` (
  `id` int(11) NOT NULL,
  `import_type` int(11) NOT NULL,
  `file_name` varchar(256) NOT NULL,
  `upload_date` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_importer`
--

INSERT INTO `user_importer` (`id`, `import_type`, `file_name`, `upload_date`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'usuarios_votantes.xlsx', '2020-06-21 17:48:53', 2, '2020-06-21 17:48:53', '2020-06-21 17:48:53'),
(2, 1, 'usuarios_votantes.xlsx', '2020-06-22 12:31:28', 2, '2020-06-22 12:31:28', '2020-06-22 12:31:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `identification_type` int(11) NOT NULL,
  `identification` int(20) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `birthdate` date NOT NULL,
  `gender` char(1) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `custom_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_info`
--

INSERT INTO `user_info` (`id`, `identification_type`, `identification`, `first_name`, `last_name`, `birthdate`, `gender`, `phone`, `email`, `address`, `custom_data`, `created_at`, `updated_at`) VALUES
(1, 1, 1085933225, 'Daniel Esteban', 'Burbano Salas', '1993-10-23', 'M', '3172199490', 'danielburbano19@gmail.com', 'Cll 73HN # 1 -15', '', '2020-06-14 21:05:18', '0000-00-00 00:00:00'),
(10, 1, 123456, 'Juan Eduardo', 'Perez Villota', '1998-12-24', 'M', '3121232323', 'test@test.com', 'Cll 1 2 #15-2', '', '2020-06-14 21:20:35', '2020-06-14 21:20:35'),
(61, 1, 12345678, 'Juan Camilo', 'Perez Salas', '1993-10-02', 'M', '312222222', 'test@test.com', 'cll 1 2 3', NULL, '2020-06-22 12:31:28', '2020-06-22 12:31:28'),
(62, 2, 87654321, 'Pedro Anibal', 'Paz Delgado', '1999-12-02', 'M', '313333333', 'test@test.com', 'cll 1 2 3', NULL, '2020-06-22 12:31:28', '2020-06-22 12:31:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_session`
--

CREATE TABLE `user_session` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_session`
--

INSERT INTO `user_session` (`id`, `user_id`, `start_date`, `end_date`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 2, '2020-06-21 21:16:48', '2020-06-28 21:16:48', '172.13.1.32', '2020-06-21 21:16:48', '2020-06-21 21:16:48'),
(2, 2, '2020-06-22 11:41:24', '2020-06-29 11:41:24', '172.13.1.32', '2020-06-22 11:41:24', '2020-06-22 11:41:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_voting_process`
--

CREATE TABLE `user_voting_process` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `voting_process_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_voting_process`
--

INSERT INTO `user_voting_process` (`id`, `user_id`, `voting_process_id`, `created_at`, `updated_at`) VALUES
(7, 27, 18, '2020-06-22 12:31:28', '2020-06-22 12:31:28'),
(8, 28, 18, '2020-06-22 12:31:28', '2020-06-22 12:31:28'),
(10, 30, 18, '2020-06-22 12:52:32', '2020-06-22 12:52:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `voting_process`
--

CREATE TABLE `voting_process` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `image` varchar(256) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `voting_process`
--

INSERT INTO `voting_process` (`id`, `name`, `description`, `start_date`, `end_date`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
(18, 'Proceso de votación para representante estudiantil ', 'Proceso de votación para representante estudiantil  2020', '2020-06-12 08:00:00', '2020-06-12 18:00:00', NULL, 2, '2020-06-22 12:31:27', '2020-06-22 12:31:27');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `grant_table`
--
ALTER TABLE `grant_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`);

--
-- Indices de la tabla `identification_type`
--
ALTER TABLE `identification_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `import_type`
--
ALTER TABLE `import_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `role_grant_table`
--
ALTER TABLE `role_grant_table`
  ADD KEY `role_id` (`role_id`),
  ADD KEY `grant_id` (`grant_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD KEY `info` (`info`),
  ADD KEY `role` (`role`);

--
-- Indices de la tabla `user_importer`
--
ALTER TABLE `user_importer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `import_type` (`import_type`);

--
-- Indices de la tabla `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `identification_type` (`identification_type`);

--
-- Indices de la tabla `user_session`
--
ALTER TABLE `user_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `user_voting_process`
--
ALTER TABLE `user_voting_process`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user` (`user_id`),
  ADD KEY `fk_voting_process` (`voting_process_id`);

--
-- Indices de la tabla `voting_process`
--
ALTER TABLE `voting_process`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `grant_table`
--
ALTER TABLE `grant_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `identification_type`
--
ALTER TABLE `identification_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `import_type`
--
ALTER TABLE `import_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `module`
--
ALTER TABLE `module`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT de la tabla `user_importer`
--
ALTER TABLE `user_importer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `user_session`
--
ALTER TABLE `user_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user_voting_process`
--
ALTER TABLE `user_voting_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `voting_process`
--
ALTER TABLE `voting_process`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `grant_table`
--
ALTER TABLE `grant_table`
  ADD CONSTRAINT `grant_table_ibfk_1` FOREIGN KEY (`module`) REFERENCES `module` (`id`);

--
-- Filtros para la tabla `role_grant_table`
--
ALTER TABLE `role_grant_table`
  ADD CONSTRAINT `role_grant_table_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`),
  ADD CONSTRAINT `role_grant_table_ibfk_2` FOREIGN KEY (`grant_id`) REFERENCES `grant_table` (`id`);

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`info`) REFERENCES `user_info` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

--
-- Filtros para la tabla `user_importer`
--
ALTER TABLE `user_importer`
  ADD CONSTRAINT `user_importer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_importer_ibfk_2` FOREIGN KEY (`import_type`) REFERENCES `import_type` (`id`);

--
-- Filtros para la tabla `user_info`
--
ALTER TABLE `user_info`
  ADD CONSTRAINT `user_info_ibfk_1` FOREIGN KEY (`identification_type`) REFERENCES `identification_type` (`id`);

--
-- Filtros para la tabla `user_session`
--
ALTER TABLE `user_session`
  ADD CONSTRAINT `user_session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `user_voting_process`
--
ALTER TABLE `user_voting_process`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_voting_process` FOREIGN KEY (`voting_process_id`) REFERENCES `voting_process` (`id`);

--
-- Filtros para la tabla `voting_process`
--
ALTER TABLE `voting_process`
  ADD CONSTRAINT `voting_process_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
