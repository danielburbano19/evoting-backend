<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://localhost/evoting/public/css/bootstrap.min.css">
    <title>Electoral results</title>
</head>

<body>
    <div class="container">
        <div class="heading">
            <center>
                <h1>Resultados electorales</h1>
                <h3>{{$resultsData['voting_process']}}</h3>
            </center>
        </div>

        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Total de votos</th>
                    <th scope="col">Número de candidatos</th>
                    <th scope="col">Número de votantes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$resultsData['total_votes']}}</td>
                    <td>{{$resultsData['candidates_number'] - 1}}</td>
                    <td>{{$resultsData['voters_number']}}</td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Porcentaje votantes activos</th>
                    <th scope="col">Porcentaje de abstencionismo</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$resultsData['active_voters_porcentage']}}%</td>
                    <td>{{$resultsData['abstentionism_voters_porcentage']}}%</td>
                </tr>
            </tbody>
        </table>

        <table class="table table-bordered">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Votos en blanco</th>
                    <th scope="col">Porcentaje votos en blanco</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$resultsData['blank_votes']}}</td>
                    <td>{{$resultsData['blank_votes_porcentage']}}%</td>
                </tr>
            </tbody>
        </table>

        <center>
            <h3>Resultados por candidato</h3>
        </center>

        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Número de votos</th>
                    <th scope="col">Porcentaje de votos</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($resultsData['results_by_candidate'] as $key => $candidate)
                @if ($candidate['first_name'] != "Voto en blanco")
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$candidate['first_name']}}</td>
                    <td>{{$candidate['last_name']}}</td>
                    <td>{{$candidate['votes_count']}}</td>
                    <td>{{$candidate['votes_porcentage']}}%</td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>

    </div>
</body>

</html>