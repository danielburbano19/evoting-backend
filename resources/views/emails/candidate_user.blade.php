<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Voters Reply</title>
</head>

<body>
    <?php
    $params = json_decode($json, true);    
    ?>
    <p>Cordial saludo {{$params['first_name']}} {{$params['last_name']}}</p><br>
    <p>Su postulación como candidato para el proceso de votación: <b>{{$params['voting_process']}}</b> ha sido creado correctamente, a continuación los detalles:</p>    
    <p>Credenciales de acceso:</p>
    <p><b>Nombre de usuario: </b> {{$params['user_name']}}</p>
    <p><b>Contraseña: </b> {{$params['password']}}</p>
   
    <br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>