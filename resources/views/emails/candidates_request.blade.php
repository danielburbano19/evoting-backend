<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Candidate Request</title>
</head>

<body>
    <p>Cordial saludo</p><br><br>
    <p>Hemos recibido la siguiente novedad del siguiente candidato:</p> 
    <?php
    $params = json_decode($json, true);
    ?>
    <p><b>Número de radicado: </b> {{$params['candidate_request_id']}}</p>
    <p><b>Identificación: </b> {{$params['identification']}}</p>
    <p><b>Nombres: </b> {{$params['first_name']}}</p>
    <p><b>Apellidos: </b> {{$params['last_name']}}</p>    
    <p><b>Correo: </b> {{$params['email']}}</p>        
    <p><b>Comentarios: </b> {{$params['comments']}}</p>
    <p><b>Nombre del proceso de votación: </b> {{$params['voting_process_name']}}</p>
    <p>Por favor revisar el caso notificado.</p><br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>
