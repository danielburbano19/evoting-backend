<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Candidate Request</title>
</head>

<body>
    <?php
    $params = json_decode($json, true);
    ?>
    <p>Cordial saludo</p><br><br>
    <p>Hemos recibido la novedad con radicado número: {{$params['candidate_request_id']}}, a continuación los detalles:</p>
    <p><b>Identificación: </b> {{$params['identification']}}</p>
    <p><b>Nombres: </b> {{$params['first_name']}}</p>
    <p><b>Apellidos: </b> {{$params['last_name']}}</p>
    <p><b>Correo: </b> {{$params['email']}}</p>
    <p><b>Comentarios: </b> {{$params['comments']}}</p>
    <p><b>Nombre del proceso de votación: </b> {{$params['voting_process_name']}}</p>
    <p>Notificaremos por este medio o se podra consultar el estado de la solicitud en la plataforma donde se genero.</p><br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>