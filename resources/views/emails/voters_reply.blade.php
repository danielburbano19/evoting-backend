<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Voters Reply</title>
</head>

<body>
    <?php
    $params = json_decode($json, true);
    switch ($params['status']) {
        case 1:
            $params['status_desc'] = "En proceso";
            break;
        default:
            $params['status_desc'] = "Revisado";
    }
    ?>
    <p>Cordial saludo {{$params['first_name']}} {{$params['last_name']}}</p><br>
    <p>Queremos notificar el estado de la novedad registrada con número de radicado: {{$params['voter_request_id']}}, para el proceso ve votación:</p>
    <p><b>{{$params['voting_process']}}</b></p>
    <p>Acontinuación los detalles para su solicitud:</p><br>
    <p><b>Estado: </b> {{$params['status_desc']}}</p>
    <?php
    if (isset($params['reply'])) {
        echo "<p>" . $params['reply'] . "</p>";
    }
    ?>
    <br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>