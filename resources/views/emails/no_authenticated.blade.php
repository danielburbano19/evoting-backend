<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>No authenticated</title>
</head>

<body>
    <?php
    $params = json_decode($json, true);    
    ?>
    <p>Cordial saludo</p><br>
    <p>Se registro un intento de ingreso no autorizado para el proceso de votación {{$params['voting_process']}}.</p>    
    <p>Desde la IP {{$params['ip']}}</p>    
    <br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>