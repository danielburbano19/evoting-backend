<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Invitation to vote</title>
</head>

<body>
    <?php
    $params = json_decode($json, true);    
    ?>
    <p>Cordial saludo</p><br>
    <p>Lo invitamos a participar en el poceso democrático de {{$params['voting_process']}}, ejerciendo su derecho al voto.</p>    
    <p>El proceso estará activo a partir de {{$params['start_date']}} hasta {{$params['end_date']}}.</p>
    <p>No dejes que otros elijan por ti.</p>      
    <br>
    <p>Saludos,</p>
    <h4>kybernan - Equipo Evoting System</h4>
    <h4>Email contact: evoting.udenar@gmail.com</h4>
    <h4>Phone Contact: 777123</h4>
</body>

</html>