<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <link rel="stylesheet" href="http://localhost/evoting/public/css/bootstrap.min.css">
    <title>Electoral results</title>
</head>

<body>
    <div class="container">
        <div class="heading">
            <center>
                <h1>Candidatos electorales</h1>
                <h3>{{$realCandidates['voting_process']}}</h3>
            </center>
        </div>       


        <table class="table table-bordered table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Identificación</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>                    
                </tr>
            </thead>
            <tbody>               
                @foreach ($realCandidates['candidates'] as $key => $candidate)
                <tr>
                    <td>{{$key}}</td>
                    <td>{{$candidate['identification']}}</td>
                    <td>{{$candidate['first_name']}}</td>
                    <td>{{$candidate['last_name']}}</td>                                        
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</body>

</html>