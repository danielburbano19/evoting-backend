<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autenticación del votante</title>
    <link rel="stylesheet" href="http://localhost/evoting/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://localhost/evoting/public/css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="http://localhost/evoting/public/css/animate.css">
</head>

<body>
    <div class="text-center row mb-3">
        <div class="col-2">
            <img src="http://localhost/evoting/public/img/app_icon.png" alt="">
        </div>
        <div class="col-10">
            <h3>Por favor, acérquese a la cámara</h3>
            <p class="font-italic">espere un momento para ser reconocido.</p>
        </div>
    </div>
    <div class="div-video">
        <video id="video" width="700" height="450" autoplay muted></video>
        <div id="voting_process_id" hidden><?php echo $processId ?></div>
    </div>
    <script defer src="http://localhost/evoting/public/js/face-api.min.js"></script>
    <script defer src="http://localhost/evoting/public/js/script.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
</body>

</html>