<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="http://localhost/evoting/public/css/bootstrap.min.css">
    <title>Voting certificates</title>
</head>

<body>
    <div class="container">
        <div class="heading">
            <center>
                <h1>Certificado de votación</h1>
                <h3>{{$data['voting_process']}}</h3>
            </center>
        </div>


        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Identificación</th>
                    <th scope="col">Nombres</th>
                    <th scope="col">Apellidos</th>
                    <th scope="col">Fecha y hora</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$data['identification']}}</td>
                    <td>{{$data['first_name']}}</td>
                    <td>{{$data['last_name']}}</td>
                    <td>{{$data['date']}}</td>
                </tr>
            </tbody>
            <div class="row">
                <div class="col-6">
                    <img src="img/firma.jpg" alt="" width="220">
                </div>
                <div class="offset-9">
                    <?php $qrCode = \QrCode::generate($data['certificateNumber']); ?>
                    <img src="data:image/svg+xml;base64,{{ base64_encode($qrCode) }}">
                </div>
            </div>
        </table>
    </div>
</body>

</html>