<?php
namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class VotingProcessTest extends TestCase
{

    use WithoutMiddleware;

    /**     
     * @test
     * @return void
     */
    public function getVotingProcesses()
    {
        $response = $this->get('api/voting_process');                
        $response->assertStatus(200);
    }
}
