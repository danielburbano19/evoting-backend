<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UserTest extends TestCase
{        
    use WithoutMiddleware;
   
    /**     
     * @test
     * @return void
     */
    public function login()
    {

        $params = array("json" => '{"user_name": "dburbano","password": "123","ip_address": "172.13.1.32"}');        
        $response = $this->postJson('api/user/login', $params);
        $response
            ->assertStatus(200);            
    }

    /**     
     * @test
     * @return void
     */
    public function getusers(){
        //$this->withoutExceptionHandling();
        //$this->withoutMiddleware();
        $response = $this->get('api/user');                
        $response->assertStatus(200);

    }

    /**     
     * @test
     * @return void
     */
    public function getuserById(){                
        $response = $this->get('api/user/1');                
        $response->assertStatus(200);

    }
}