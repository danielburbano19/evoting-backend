<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'candidates';

    //many-to-many relationships
    public function candidates_voting_processes()
    {
        return $this->belongsToMany('App\Voting_process', 'candidates_voting_processes');
    }

    //one-to-one relationshipssss
    public function electoral_proposal(){
        return $this->hasOne('App\Electoral_proposal', 'candidate_id');
    }
    
    //one-to-many relationships
    public function electoral_votes()
    {
        return $this->hasMany('App\Electoral_vote','candidate_id');
    }

    //one-to-many relationships
    public function candidate_requests()
    {
        return $this->hasMany('App\Candidate_request','candidate_id');
    }
}
