<?php

namespace App\Http\Middleware;

use Closure;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\Log;

class ApiAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //validate token
        $token = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($token);
        if ($checkToken) {
            //validate role in request
            $uri = $request->getUri();
            $method = $request->getMethod();
            switch ($method) {
                case 'POST':
                    $grantType = 'C';
                    break;
                case 'GET':
                    $grantType = 'R';
                    break;
                case 'PUT':
                    $grantType = 'U';
                    break;
                case 'DELETE':
                    $grantType = 'D';
                    break;
                default:
                    $grantType = "ERROR";
            }
            if ($grantType == "ERROR") {
                //create log emergency
                $log = [
                    'URI' => $request->getUri(),
                    'METHOD' => $request->getMethod(),
                    'REQUEST_BODY' => $request->all(),
                ];
                Log::channel('evoting')->emergency($log);

                $data = array(
                    'status' => 'Error',
                    'code' => '401',
                    'message' => 'Usuario no autorizado',
                );
            } else {
                $checkRole = $jwtAuth->checkRole($token, $uri, $grantType);
                if ($checkRole) {
                    return $next($request);
                } else {
                    //create log emergency
                    $log = [
                        'URI' => $request->getUri(),
                        'METHOD' => $request->getMethod(),
                        'REQUEST_BODY' => $request->all(),
                    ];
                    Log::channel('evoting')->emergency($log);

                    $data = array(
                        'status' => 'Error',
                        'code' => '401',
                        'message' => 'Usuario no autorizado',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '401',
                'message' => 'Usuario no identificado',
            );
        }
        return response()->json($data, $data['code']);
    }
}
