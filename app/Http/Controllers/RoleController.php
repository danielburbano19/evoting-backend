<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::all();

        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'role' => $role
        ]);
    }

    public function show($id)
    {
        $role = Role::find($id);
        if (is_object($role)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'role' => $role,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error rol no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'name' => 'required|unique:roles,name,{$params["name"]},id,deleted_at,NULL',                
                'description' => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear el rol',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $role = new Role();
                $role->name = $params['name'];
                $role->description = $params['description'];
                $role->save();

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'role' => $role,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el rol correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {

            //validate data
            $validate = Validator::make($params, [
                'name' => 'required|unique:roles,name,{$params["name"]},id,deleted_at,NULL',
                'description' => 'required',
            ]);

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar el rol',
                    'errors' => $validate->errors(),
                );
            } else {

                //unset data to not update
                unset($params['id']);
                unset($params['created_at']);

                //update register
                $role = Role::find($id);
                if (is_object($role)) {
                    $role = Role::where('id', $id)->update($params);
                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'role' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error role no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el rol correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $role = Role::find($id);

        //check if register exist
        if (!empty($role)) {
            $role->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'role' => $role,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'El rol no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }   
}
