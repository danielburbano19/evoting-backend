<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Candidate_request;
use App\Voting_process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use stdClass;

class CandidateRequestController extends Controller
{
    public function index()
    {
        $candidateRequest = Candidate_request::all();

        if (is_object($candidateRequest)) {
            $candidateRequest->each(function ($reg) {
                $votingProcess = Voting_process::find($reg->voting_process_id);
                $reg->voting_process_name = $votingProcess->name;
            });
        }
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'candidateRequest' => $candidateRequest
        ]);
    }

    public function show($id)
    {
        $candidateRequest = Candidate_request::find($id);
        if (is_object($candidateRequest)) {
            $votingProcess = Voting_process::find($candidateRequest->voting_process_id);
            $candidateRequest->voting_process_name = $votingProcess->name;
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'candidateRequest' => $candidateRequest,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la solicitud no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'candidate_id' => 'required',
                'comments' => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear la novedad para ser enviada con el area correspondiente',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $candidateRequest = new Candidate_request();
                $candidateRequest->candidate_id = $params['candidate_id'];
                $candidateRequest->voting_process_id = $params['voting_process_id'];
                $candidateRequest->comments = $params['comments'];

                $votingProcess = Voting_process::find($params['voting_process_id']);
                $candidate = Candidate::find($params['candidate_id']);
                if (is_object($votingProcess) && is_object($candidate)) {
                    $candidateRequest->save();
                    $params['voting_process_name'] = $votingProcess->name;
                    $params['candidate_request_id'] = $candidateRequest->id;
                    $params['identification'] = $candidate->identification;
                    $params['first_name'] = $candidate->first_name;
                    $params['last_name'] = $candidate->last_name;
                    $GLOBALS["email"] = $candidate->email;
                    $params['email'] = $candidate->email;
                    $paramsMail = array("json" => json_encode($params));

                    //send email to admin
                    Mail::send('emails.candidates_request', $paramsMail, function ($message) {
                        $message->subject('Solicitud de novedad candidato');
                        $message->cc(['franciscozambrano@outlook.es']);
                        $message->to('danielburbano19@gmail.com');
                    });

                    //send email to user
                    Mail::send('emails.candidates_request_candidate', $paramsMail, function ($message) {
                        $message->subject('Registro solicitud de novedad canidato');
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->to($GLOBALS["email"]);
                    });


                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'candidateRequest' => $candidateRequest,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'El ID del proceso de votación o el id del candidato enviado no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la novedad correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function getCandidateRequest($identification)
    {
        $candidateRequest = Candidate_request::where('identification', $identification)->get();
        if (is_object($candidateRequest)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'candidateRequest' => $candidateRequest,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la solicitud no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function reply(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {

            //validate data
            $validate = Validator::make($params, [
                'candidate_request_id' => 'required',
                'status' => 'required|integer|between:1,2'
            ]);

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar el estado de la novedad reportada',
                    'errors' => $validate->errors(),
                );
            } else {

                //update fields
                $candidateRequest = Candidate_request::find($params['candidate_request_id']);
                if (is_object($candidateRequest)) {
                    $candidateRequest->status = $params['status'];
                    $candidate = Candidate::find($candidateRequest->candidate_id);
                    $GLOBALS["email"] = $candidate->email;

                    $votingProcess = Voting_process::find($candidateRequest->voting_process_id);
                    $data = new stdClass();
                    $data->candidate_request_id = $params['candidate_request_id'];
                    $data->voting_process = $votingProcess->name;
                    $data->first_name = $candidate->first_name;
                    $data->last_name = $candidate->last_name;
                    $data->status = $params['status'];
                    if (isset($params['reply'])) {
                        $data->reply = $params['reply'];
                        $candidateRequest->reply = $params['reply'];
                    }
                    $paramsMail = array("json" => json_encode($data));

                    $candidateRequest->save();

                    Mail::send('emails.candidates_reply', $paramsMail, function ($message) {
                        $message->subject('Respuesta a la novedad Vote - APP');
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->to($GLOBALS["email"]);
                    });

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'candidateRequest' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la novedad que se intenta responder no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el rol correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
