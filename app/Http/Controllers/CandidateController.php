<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\Electoral_proposal;
use App\Candidate_voting_process;
use App\Role;
use App\User;
use App\Voting_process;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use stdClass;

class CandidateController extends Controller
{
    public function index()
    {
        //get candidates and proposals     
        $candidate = Candidate::all()->each(function ($reg) {
            $elctoralProposals = Electoral_proposal::where("candidate_id", $reg->id)->first();
            //validate if candidate have proposals
            if (is_object($elctoralProposals)) {
                //add proposals to candidate object
                $reg->proposals = json_decode($elctoralProposals->proposals);
            } else {
                $reg->proposals = "";
            }
        });
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'candidate' => $candidate
        ]);
    }

    public function show($id)
    {
        $candidate = Candidate::find($id);
        if (is_object($candidate)) {
            //get proposals
            $electoralProposals = $candidate->electoral_proposal;
            //validate if candidate have proposals
            if (is_object($electoralProposals)) {
                //add proposals to candidate object
                $candidate->proposals = json_decode($electoralProposals->proposals);
            } else {
                $candidate->proposals = "";
            }
            //delete field to show
            unset($candidate->electoral_proposal);
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'candidate' => $candidate
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del candidato no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);

        //get voting process image if exist
        $CandidateImage = $request->file('file0');

        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'identification' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birthdate' => 'required',
                'gender' => 'required',
                'email' => 'required|email',
                'voting_process_id' => 'required',
                'proposals' => 'required',
            ]);

            if ($CandidateImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado el candidato',
                    'errors' => $validate->errors(),
                );
            } else {
                $candidateVotingProcess = "";
                $candidate = Candidate::where('identification', $params['identification'])->first();
                if (is_object($candidate)) {
                    $candidateVotingProcess = Candidate_voting_process::where('candidate_id', $candidate->id)
                        ->where('voting_process_id', $params['voting_process_id'])
                        ->first();
                }

                if (is_object($candidateVotingProcess)) {
                    //candidate and voting_process already exist
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se pudo crear el candidato, debido a que ya existe para el proceso de votación',
                    );
                } else {

                    //create a new candidate
                    $candidate = new Candidate();
                    $candidate->identification = $params['identification'];
                    $candidate->first_name = $params['first_name'];
                    $candidate->last_name = $params['last_name'];
                    $candidate->birthdate = $params['birthdate'];
                    $candidate->gender = $params['gender'];
                    $candidate->email = $params['email'];
                    if (isset($params['description'])) {
                        $candidate->description = $params['description'];
                    }
                    $candidate->save();

                    //create proposals to candidate
                    $electoralProposals = new Electoral_proposal();
                    $electoralProposals->candidate_id = $candidate->id;
                    $electoralProposals->proposals = json_encode($params['proposals']);
                    $electoralProposals->save();

                    //asociate cadidate to voting_process                
                    $candidateVotingProcess = new Candidate_voting_process();
                    $candidateVotingProcess->candidate_id = $candidate->id;
                    $candidateVotingProcess->voting_process_id = $params['voting_process_id'];
                    $candidateVotingProcess->save();

                    //create candidate role if not exist
                    $role = Role::where('name', 'Candidate')->first();
                    if (!is_object($role)) {
                        $role = new Role();
                        $role->name = "Candidate";
                        $role->description = "Rol con permisos de candidato para acceso al sistema";
                        $role->save();
                    }

                    //create user in system to candidate
                    $pwdGenerate = $this->generatePassword();
                    $pwd = hash('sha256', $pwdGenerate);
                    $user = new User();
                    $user->identification = $params['identification'];
                    $user->first_name = $params['first_name'];
                    $user->last_name = $params['last_name'];
                    $user->birthdate = $params['birthdate'];
                    $user->gender = $params['gender'];
                    $user->email = $params['email'];
                    $user->custom_data = json_encode(array('voting_process_id' => $params['voting_process_id'], 'candidate_id' => $candidate->id));
                    $GLOBALS["email"] = $params['email'];

                    $user->role_id = $role->id;
                    $user->user_name = $params['voting_process_id'] . "_" . $params['identification'];
                    $user->password = $pwd;
                    $user->save();

                    if ($CandidateImage) {
                        $imageName = $params['voting_process_id'] . "_" . $params['identification'] . ".jpg";
                        //save image candidate
                        Storage::disk('candidates')->put($imageName, File::get($CandidateImage));
                        $candidate->image = $imageName;
                        //save image user
                        Storage::disk('users')->put($params['identification'] . ".jpg", File::get($CandidateImage));
                    }

                    $votingProcess = Voting_process::find($params['voting_process_id']);
                    $dataEmail['voting_process'] = $votingProcess->name;
                    $dataEmail['first_name'] = $params['first_name'];
                    $dataEmail['last_name'] = $params['last_name'];
                    $dataEmail['identification'] = $params['identification'];
                    $dataEmail['user_name'] = $user->user_name;
                    $dataEmail['password'] = $pwdGenerate;
                    $paramsMail = array("json" => json_encode($dataEmail));

                    //send email to user
                    Mail::send('emails.candidate_user', $paramsMail, function ($message) {
                        $message->subject('Registro candidatura');
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->to($GLOBALS["email"]);
                    });

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'candidate' => $candidate,
                        'electoral_proposals' => $electoralProposals
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del candidato correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        //get voting process image if exist
        $CandidateImage = $request->file('file0');

        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'first_name' => 'required',
                'last_name' => 'required',
                'birthdate' => 'required',
                'gender' => 'required',
                'email' => 'required|email',
            ]);

            if ($CandidateImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar la información del candidato',
                    'errors' => $validate->errors(),
                );
            } else {
                //get register            
                $candidate = Candidate::find($id);

                //check if register exist
                if (is_object($candidate)) {
                    //unset data to not update
                    unset($params['id']);
                    unset($params['created_at']);
                    if (isset($params['description'])) {
                        $candidate->description = $params['description'];
                    } else {
                        unset($params['description']);
                    }

                    $tempIdentification =$candidate->identification;
                    unset($params['identification']);
                    //update register
                    $candidate = Candidate::where('id', $id)->update($params);

                    //update image if exist
                    if ($CandidateImage) {                        
                        Candidate::find($id)->candidates_voting_processes->each(function ($reg) {
                            $GLOBALS["votingProcessId"] = $reg->id;
                        });
                        $imageName = $GLOBALS["votingProcessId"] . "_" . $tempIdentification . ".jpg";
                        //save image
                        Storage::disk('candidates')->put($imageName, File::get($CandidateImage));
                        $params['image'] = $imageName;
                    }
                                        
                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'candidate' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la información del candidato no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del candidato correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $candidate = Candidate::find($id);

        //check if register exist
        if (!empty($candidate)) {

            //delete candidate proposals 
            $cadidateProposals = $candidate->electoral_proposal;
            if (!empty($cadidateProposals)) {
                $cadidateProposals->delete();
            }

            //soft delete candidate_voting_prosecess pivot
            DB::table('candidates_voting_processes')
                ->where('candidate_id', $id)
                ->update(array('deleted_at' => DB::raw('NOW()')));

            $candidate->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'candidate' => $candidate,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'La información del candidato no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }

    public function getImage($fileName)
    {
        $isset = Storage::disk('candidates')->exists($fileName . ".jpg");
        if ($isset) {
            $file = Storage::disk('candidates')->get($fileName . ".jpg");
            return new Response($file, 200);
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Imagen no encontrada',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function generatePassword()
    {
        $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#@';
        for ($pass = '', $n = strlen($caracteres) - 1; strlen($pass) < 8;) {
            $x = rand(0, $n);
            $pass .= $caracteres[$x];
        }
        return $pass;
    }
}
