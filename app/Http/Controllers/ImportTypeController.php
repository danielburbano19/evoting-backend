<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Import_type;
use Illuminate\Support\Facades\Validator;

class ImportTypeController extends Controller
{
    public function index()
    {
        $import_type = Import_type::all();

        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'import_type' => $import_type
        ]);
    }

    public function show($id)
    {
        $import_type = Import_type::find($id);
        if (is_object($import_type)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'import_type' => $import_type,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error Tipo de importación no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'name' => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear el tipo de importación',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $identification_type = new Import_type();
                $identification_type->name = $params['name'];
                $identification_type->save();

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'identification_type' => $identification_type,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el tipo de importación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {

            //validate data
            $validate = Validator::make($params, [
                'name' => 'required',
            ]);

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar el tipo de importación',
                    'errors' => $validate->errors(),
                );
            } else {

                //unset data to not update
                unset($params['id']);

                //update register
                $import_type = Import_type::find($id);
                if (is_object($import_type)) {
                    $import_type = Import_type::where('id', $id)->update($params);

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'identification_type' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error Tipo de importación no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el tipo de importación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        /*  //get register
        $identification_type = Import_type::find($id);

        //check if register exist
        if (!empty($identification_type)) {            
            $identification_type->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'identification_type' => $identification_type,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'El tipo de importación no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']); */
    }
}
