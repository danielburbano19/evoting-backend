<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_importer;

class UserImporterController extends Controller
{
    public function index()
    {
        $user_importer = User_importer::all();

        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'user_importer' => $user_importer
        ]);
    }

    public function show($id)
    {
        $user_importer = User_importer::find($id);
        if (is_object($user_importer)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'user_importer' => $user_importer,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la importación de usuarios no existe',
            );
        }
        return response()->json($data, $data['code']);
    }    

    public function destroy($id, Request $request)
    {    
        //get register
        $identification_type = User_importer::find($id);

        //check if register exist
        if (!empty($identification_type)) {            
            $identification_type->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'identification_type' => $identification_type,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'El tipo de importación no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }
}
