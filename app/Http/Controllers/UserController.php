<?php

namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\User;
use App\User_session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'user' => json_decode($user)
        ]);
    }

    public function show($id)
    {
        $user = User::find($id);    
        if (is_object($user)) {
            $user->custom_data = json_decode($user->custom_data);
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'user' =>$user
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del usuario no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json

        //get user image if exist
        $userImage = $request->file('file0');

        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'identification' => 'required|unique:users,identification,{$params["identification"]},id,deleted_at,NULL',
                'first_name' => 'required',
                'last_name' => 'required',
                'birthdate' => 'required',
                'gender' => 'required',
                'email' => 'required|email',
                'role_id' => 'required',
                'user_name' => 'required|unique:users,deleted_at,NULL',
                'password' => 'required'
            ]);

            if ($userImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado el usuario',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $pwd = hash('sha256', $params['password']);

                /* $password =  $params['password'];
                $iterations = 1000;                
                $salt = openssl_random_pseudo_bytes(16);
                $pwd = hash_pbkdf2("sha256", $password, $salt, $iterations, 20); */

                $user = new User();
                $user->identification = $params['identification'];
                $user->first_name = $params['first_name'];
                $user->last_name = $params['last_name'];
                $user->birthdate = $params['birthdate'];
                $user->gender = $params['gender'];
                $user->email = $params['email'];
                if (isset($params['custom_data'])) {
                    $user->custom_data = $params['custom_data'];
                }
                $user->role_id = $params['role_id'];
                $user->user_name = $params['user_name'];
                $user->password = $pwd;
                $user->save();

                if ($userImage) {
                    $imageName = $user->identification . ".jpg";
                    //save image
                    Storage::disk('users')->put($imageName, File::get($userImage));
                    $user->image = $imageName;
                }

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'user' => $user,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del usuario correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        //get user image if exist
        $userImage = $request->file('file0');

        if (!empty($params)) {
            //get register
            $user = User::find($id);
            $errorValidation = false;

            //check if register exist
            if (is_object($user)) {
                //validate data
                $existUser = User::where('identification', $params['identification'])->first();
                if (is_object($existUser) && $user->identification != $params['identification']) {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la identificación que desea modificar ya esta asignada a otro usuario',
                    );
                } else {
                    $validate = Validator::make($params, [
                        'identification' => 'required',
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'birthdate' => 'required',
                        'gender' => 'required',
                        'email' => 'required|email',
                        'role_id' => 'required',
                    ]);
                    $errorValidation = $validate->fails();

                    //validate unique username
                    if ($params['user_name']) {
                        if ($user->user_name != $params['user_name']) {
                            if (!$errorValidation) {
                                $validate = Validator::make($params, [
                                    'user_name' => 'unique:users',
                                ]);
                                $errorValidation = $validate->fails();
                            }
                        }
                    } else {
                        //username is required
                        $data = array(
                            'status' => 'Error',
                            'code' => '400',
                            'message' => 'No se ha logrado actualizar la información del usuario debido a que falta el nombre de usuario',
                        );
                    }

                    //validate userImage
                    if ($userImage) {
                        if (!$errorValidation) {
                            $validate = Validator::make($request->all(), [
                                'file0' => 'required|mimes:jpg,jpeg'
                            ]);
                            $errorValidation = $validate->fails();
                        }
                    }

                    if ($errorValidation) {
                        //json with errors in validation data
                        $data = array(
                            'status' => 'Error',
                            'code' => '400',
                            'message' => 'No se ha logrado actualizar la información del usuario',
                            'errors' => $validate->errors(),
                        );
                    } else {

                        //unset data to not update
                        unset($params['id']);
                        unset($params['created_at']);
                        if (!isset($params['password'])) {
                            unset($params['password']);
                        } else {
                            $params['password'] = hash('sha256', $params['password']);
                        }
                        if (!isset($params['custom_data'])) {
                            unset($params['custom_data']);
                        }
                        //update register
                        $user = User::where('id', $id)->update($params);

                        //update image if exist
                        if ($userImage) {
                            $imageName = $params['identification'] . ".jpg";
                            //save image
                            Storage::disk('users')->put($imageName, File::get($userImage));
                            $params['image'] = $imageName;
                        }

                        //dont return password
                        unset($params['password']);
                        $data = array(
                            'status' => 'Success',
                            'code' => '200',
                            'user' => $params,
                        );
                    }
                }
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'Error la información del usuario no existe',
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del usuario correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $user = User::find($id);

        //check if register exist
        if (!empty($user)) {
            $user->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'user' => $user,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'La información del usuario no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }

    public function login(Request $request)
    {
        $jwt = new JwtAuth();
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'user_name' => 'required',
                'password' => 'required',
                'ip_address' => 'required|ip'
            ]);

            //check user credentials 
            if ($validate->fails()) {
                //json with errors in validation data
                $signup = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha enviado los datos de login correctamente',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $pwd = hash('sha256', $params['password']);
                $token = $jwt->signup($params['user_name'], $pwd);
                if ($token) {

                    $user_decoded = $jwt->signup($params['user_name'], $pwd, true);
                    $userId = $user_decoded->sub;
                    $tokenExpires = $user_decoded->exp;

                    //create user session
                    $user_session = new User_session();
                    $user_session->user_id = $userId;
                    $user_session->start_date = date('Y-m-d H:i:s');
                    $user_session->end_date = date('Y-m-d H:i:s', $tokenExpires);
                    $user_session->ip_address = $params['ip_address'];
                    $user_session->save();

                    $role = User::find($userId)->role->id;

                    $signup = array(
                        'status' => 'Success',
                        'code' => '200',
                        'token' => $token,
                        'user_id' => $userId,
                        'role' => $role,
                    );

                    if (!empty($params['getToken'])) {
                        $user_decoded = $jwt->signup($params['user_name'], $pwd, true);
                        $signup = array(
                            'status' => 'Success',
                            'code' => '200',
                            'user_decoded' => $user_decoded,
                        );
                    }
                } else {
                    $signup = array(
                        'status' => 'Error',
                        'code' => '401',
                        'message' => 'El usuario o contraseña son incorrectos',
                    );
                }
            }
        } else {
            $signup = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del usuario correctamente',
            );
        }
        //return response
        return response()->json($signup, $signup['code']);
    }

    public function getImage($fileName)
    {
        $isset = Storage::disk('users')->exists($fileName . ".jpg");
        if ($isset) {
            $file = Storage::disk('users')->get($fileName . ".jpg");
            return new Response($file, 200);
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Imagen no encontrada',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
