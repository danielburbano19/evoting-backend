<?php

namespace App\Http\Controllers;

use App\Authentication_log;
use App\Voting_process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthenticationLogController extends Controller
{
    public function show($id)
    {
        $authenticationLog = Authentication_log::where('voting_process_id', $id)->get();
        if (count($authenticationLog)) {
            $votingProcess = Voting_process::find($id);
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voting_process' => $votingProcess->name,
                'authentication_log' => $authenticationLog,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error log de authenticación no existe para el proceso de votación',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'status' => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear el log de authenticación',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $authenticationLog = new Authentication_log();
                $authenticationLog->voting_process_id = $params['voting_process_id'];
                if ($params['voter_identification']) {
                    $authenticationLog->voter_identification = $params['voter_identification'];
                }
                if (isset($params['score'])) {
                    $authenticationLog->score = $params['score'];
                }
                $authenticationLog->status = $params['status'];
                $authenticationLog->save();

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'Authentication_log' => $authenticationLog,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el log de authenticación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
