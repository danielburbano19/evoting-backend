<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Voter;
use App\Voter_session;
use App\Voting_process;
use App\Voter_voting_process;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use App\Helpers\JwtAuth;

class VoterController extends Controller
{
    public function index()
    {
        $voter = Voter::all();
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'voter' => $voter
        ]);
    }

    public function show($id)
    {
        $voter = Voter::find($id);
        if (is_object($voter)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voter' => $voter,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del votante no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        //get voting process image if exist
        $voterImage = $request->file('file0');

        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'identification' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'birthdate' => 'required',
                'gender' => 'required',
                'email' => 'required|email',
            ]);

            if ($voterImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar la información del votante',
                    'errors' => $validate->errors(),
                );
            } else {
                //get register            
                $voter = Voter::find($id);

                //check if register exist
                if (is_object($voter)) {

                    $existUser = Voter::where('identification', $params['identification'])->first();
                    if (is_object($existUser) && $voter->identification != $params['identification']) {
                        $data = array(
                            'status' => 'Error',
                            'code' => '404',
                            'message' => 'Error la identificación que desea modificar ya esta asignada a otro votante',
                        );
                    } else {

                        //unset data to not update
                        unset($params['id']);
                        unset($params['created_at']);
                        if (!isset($params['custom_data'])) {
                            unset($params['custom_data']);
                        }
                        //update register
                        $voter = Voter::where('id', $id)->update($params);

                        if ($voterImage) {
                            $imageName = $params['identification'] . ".jpg";
                            //save image
                            Storage::disk('voters')->put($imageName, File::get($voterImage));
                            $params['image'] = $imageName;
                        }

                        $data = array(
                            'status' => 'Success',
                            'code' => '200',
                            'voter' => $params,
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la información del votante no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del votante correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $voter = Voter::find($id);

        //check if register exist
        if (!empty($voter)) {

            //delete voter proposals 
            $cadidateProposals = $voter->electoral_proposal;
            if (!empty($cadidateProposals)) {
                $cadidateProposals->delete();
            }

            //soft delete candidate_voting_prosecess pivot
            DB::table('voters_voting_processes')
                ->where('voter_id', $id)
                ->update(array('deleted_at' => DB::raw('NOW()')));

            $voter->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'voter' => $voter,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'La información del votante no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }

    public function voterEnabled($identification)
    {
        $voter = Voter::where('identification', $identification)->first();
        if (is_object($voter)) {
            $GLOBALS["votingProcessesses"] = array();
            $voter->voters_voting_processes_enabled->each(function ($reg) {
                $voting_process = Voting_process::find($reg->id, ['name', 'description', 'start_date']);
                array_push($GLOBALS["votingProcessesses"], $voting_process);
            });
            if (is_array($GLOBALS["votingProcessesses"]) && $GLOBALS["votingProcessesses"]) {
                $data = array(
                    'code' => '200',
                    'status' => 'Success',
                    'voters_voting_processes' => $GLOBALS["votingProcessesses"],
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'No existe ningun proceso de votación para el usuario consultado',
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'No existe ningun proceso de votación para el usuario consultado',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function voterRegisterdVote(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'voter_id' => 'required',
            ]);
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha enviado la información correctamente',
                    'errors' => $validate->errors(),
                );
            } else {
                $voter_voting_process = Voter_voting_process::where('voter_id', $params['voter_id'])
                    ->where('voting_process_id', $params['voting_process_id'])
                    ->first();
                if (is_object($voter_voting_process)) {
                    if ($voter_voting_process->registred_vote == "0") {
                        $data = array(
                            'code' => '200',
                            'status' => 'Success',
                            'registered_vote' => "No ha registrado su voto aún",
                        );
                    } else {
                        $data = array(
                            'code' => '200',
                            'status' => 'Success',
                            'registered_vote' => "Ya ha registrado su voto",
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'No existe ningun registro para el usuario y proceso de votación consultado',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información correctamente',
            );
        }

        return response()->json($data, $data['code']);
    }



    //dropzone.js
    //https://www.youtube.com/watch?v=W8mdizwVotw

    //http://localhost/evoting/storage/app/voters/

    public function uploadImage(Request $request)
    {
        //get data from post request
        $image = $request->file('file0');

        //validate data
        $validate = Validator::make($request->all(), [
            'file0' => 'required|mimes:jpg,jpeg'
        ]);

        if ($image && !$validate->fails()) {
            //save image
            $image_name = $image->getClientOriginalName();
            Storage::disk('voters')->put($image_name, File::get($image));
            $data = array(
                'code' => '200',
                'status' => 'success',
                'image' => $image_name

            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => '404',
                'message' => 'Error al subir la imagen',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function getImage($fileName)
    {
        $isset = Storage::disk('voters')->exists($fileName . ".jpg");
        if ($isset) {
            $file = Storage::disk('voters')->get($fileName . ".jpg");
            return new Response($file, 200);
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Imagen no encontrada',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function authenticateVoter(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'voter_identification' => 'required',
            ]);
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha enviado la información correctamente',
                    'errors' => $validate->errors(),
                );
            } else {
                $voterId = Voter::where('identification', $params['voter_identification'])->first()->id;
                $voter_voting_process = Voter_voting_process::where('voter_id', $voterId)
                    ->where('voting_process_id', $params['voting_process_id'])
                    ->first();
                if (is_object($voter_voting_process)) {
                    $voter_voting_process->authenticated = 1;
                    $voter_voting_process->save();

                    $data = array(
                        'code' => '200',
                        'status' => 'Success',
                        'registered_vote' => "Votante habilitado",
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'No existe ningun registro para el usuario y proceso de votación consultado',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información correctamente',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function checkAuthenticatedVoter(Request $request)
    {
        $jwt = new JwtAuth();

        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'voter_identification' => 'required',
                'ip_address' => 'required|ip'
            ]);
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha enviado la información correctamente',
                    'errors' => $validate->errors(),
                );
            } else {
                $votingProcess = Voting_process::find($params['voting_process_id']);
                if ($votingProcess->status == "1") {
                    $currentDate = strtotime(date("Y-m-d H:i:s", time()));
                    $startDate = strtotime($votingProcess->start_date);
                    $endDate = strtotime($votingProcess->end_date);
                    //validate if vote is in date range 
                    if ($currentDate >= $startDate && $currentDate <= $endDate) {
                        $voter = Voter::where('identification', $params['voter_identification'])->first();
                        if (is_object($voter)) {
                            $voter_voting_process = Voter_voting_process::where('voter_id', $voter->id)
                                ->where('voting_process_id', $params['voting_process_id'])
                                ->first();
                            if (is_object($voter_voting_process)) {
                                //vadate if voter is already registered your vote
                                if ($voter_voting_process->registred_vote == "0") {
                                    //correct validation
                                    $token = $jwt->signupVoter($params['voter_identification']);

                                    if ($token) {

                                        $voter_decoded = $jwt->signupVoter($params['voter_identification'], true);
                                        $VoterId = $voter_decoded->sub;
                                        $tokenExpires = $voter_decoded->exp;

                                        //create user session
                                        $voter_session = new Voter_session();
                                        $voter_session->voter_id = $VoterId;
                                        $voter_session->start_date = date('Y-m-d H:i:s');
                                        $voter_session->end_date = date('Y-m-d H:i:s', $tokenExpires);
                                        $voter_session->ip_address = $params['ip_address'];
                                        $voter_session->save();

                                        $data = array(
                                            'code' => '200',
                                            'status' => 'Success',
                                            'token' => $token,
                                            'voter_id' => $voter->id,
                                            'voter_first_name' => $voter->first_name,
                                            'voter_last_name' => $voter->last_name,
                                            'voting_process_name' => $votingProcess->name,
                                        );
                                    }
                                } else {
                                    $data = array(
                                        'status' => 'Error',
                                        'code' => '404',
                                        'message' => 'Error el votante ya ha registrado su voto',
                                    );
                                }
                            } else {
                                $data = array(
                                    'status' => 'Error',
                                    'code' => '404',
                                    'message' => 'No existe ningun registro para el usuario y proceso de votación consultado',
                                );
                            }
                        } else {
                            $data = array(
                                'status' => 'Error',
                                'code' => '404',
                                'message' => 'No existe ningun registro para el usuario y proceso de votación consultado',
                            );
                        }
                    } else {
                        $data = array(
                            'status' => 'Error',
                            'code' => '404',
                            'message' => 'Error la fecha actual es diferente al rango configurado para el proceso de votación',
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'El proceso de votación no tiene estado iniciado',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información correctamente',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function saveAuthImg()
    {
        //$imgData = file_get_contents("php://input"); //Obtener la imagen 
        $imgData = json_decode(file_get_contents('php://input'));

        if (strlen($imgData->img_64) <= 0) exit("No se recibió ninguna imagen");
        //La imagen traerá al inicio data:image/png;base64, cosa que debemos remover
        $imgEncodedClean = str_replace("data:image/png;base64,", "", urldecode($imgData->img_64));

        //Venía en base64 pero sólo la codificamos así para que viajara por la red, ahora la decodificamos y
        //todo el contenido lo guardamos en un archivo
        $imgDecoded = base64_decode($imgEncodedClean);

        //Calcular un nombre único
        //$imgName = $imgData->voting_process . "_" . $imgData->voter_id . "_" . date("Y-m-d_h:i:s") . ".png";
        $imgName = $imgData->voting_process . "_" . $imgData->voter_id . "_" . time() . ".png";
        print_r($imgName);
        //save image
        Storage::disk('voting_process_voters_auth')->put($imgName, $imgDecoded);

        //Terminar y regresar el nombre de la foto
        exit($imgName);
    }
}
