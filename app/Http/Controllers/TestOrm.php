<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Voter;
use App\Candidate;
use App\Candidate_voting_process;
use App\Electoral_proposal;
use App\Electoral_vote;
use App\Role;
use App\Voter_voting_process;
use App\Voting_process;
use App\Electoral_result;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ElectoralResultsExport;
use CreateElectoralResults;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDF;
use stdClass;

class TestOrm extends Controller
{
    public function testOrm()
    {
        /*         $users= identification_type::all();
        foreach ($users as $user ) {
            echo $user->name;
        } */
        //$user = User::find(27)->user_voting_process;
        //return $user;
        //return response(['created'=>true],200);
        //$user_info = User_info::where("identification", "1085933225")->first();
        //$users = User::where("info", $user_info->id)->get();
        //print_r($users);
        //$user = User::find(2)->role->id;
        //$voting_process = Voting_process::find(2);
        //$voting_process->voters_voting_processes()->sync(array(1 => array('deleted_at' => DB::raw('NOW()'))));;
        //$voting_process = Voting_process::find(1)->delete();
        //$candidate = Candidate::find(3)->electoral_proposal;;
        //$voting_process = Voting_process::find(2);
        //$candidate = Candidate::where('identification', '123456')->first();
        /*$votingProcess = Voting_process::find('1');
        $votingProcess2 = $votingProcess->voters_voting_processes()->each(function ($reg) {
            $voterVotingProcess = Voter_voting_process::where("voting_process_id", '1')->first();
            $reg->update(array('deleted_at' => date('Y-m-d H:i:s')));
            //print_r($reg);
        });*/

        /*$voterVotingProcess = Voter_voting_process::where("voting_process_id", '1')->get()->each(function ($reg) {
            $reg->update(array('deleted_at' => date('Y-m-d H:i:s')));
            //print_r($reg->deleted_at);
        });*/
        /*$voting_process = Voting_process::all()->each(function ($reg) {
            $candidatesVotingProcess = Voter::where("voting_process_id", $reg->id)->first();            
            if (is_object($candidatesVotingProcess)) {
                //print_r($reg->id ." si");
                $reg->candidates = 1;
            }
            else{
                //print_r($reg->id ." no");
                $reg->candidates = 0;
            }           
            //print_r($reg);
        });*/
        /*$votingProcess = Candidate::find('2')->candidates_voting_processes->each(function ($reg){
            $GLOBALS["votingProcessId"] = $reg->id;
            
        });*/
        //$candidate = Candidate::find(5);
        //$proposals = $candidate;
        /*$candidate = Candidate::all()->each(function ($reg) {
            $elctoralProposal = Electoral_proposal::where("candidate_id", $reg->id)->first();
            $reg->proposals = $elctoralProposal->proposals;                        
        });*/
        /*$GLOBALS["votingProcessesses"] = array();
        Voter::where('identification', '12345670')->first()->voters_voting_processes_enabled->each(function ($reg){
            $voting_process = Voting_process::find($reg->id, ['name', 'description', 'start_date']);            
            array_push($GLOBALS["votingProcessesses"],$voting_process);         
        });
        //print_r($GLOBALS["votingProcessesses"]);            
        /*$voterVotingProcess = Voter_voting_process::where('voter_id', '4')
                                                    ->where('voting_process_id', '1')
                                                    ->get();

        if($voterVotingProcess->isEmpty()){
            echo "esta vacio";
        }*/

        //$votesByCandidate = Electoral_vote::where('candidate_id','1')->get();
        //$votesByCandidate = Electoral_vote::all()->groupBy('candidate_id')->count();
        /*$votesByCandidate = DB::table('electoral_votes')                        
                        ->select(DB::raw('voting_process_id, candidate_id, count(*) as votes_count')) 
                        ->where('voting_process_id', 1)
                        ->groupBy('voting_process_id')
                        ->groupBy('candidate_id')
                        ->orderBy('votes_count', 'desc')               
                        ->get();
        if($votesByCandidate->isEmpty()){
            
        }
        print_r($votesByCandidate[0]->votes_count);
        

        $max_votes = DB::table('electoral_results')
                ->where('voting_process_id', 1)
                ->max('votes_count');
          
        $votesByCandidate = DB::table('electoral_results')                        
                        ->join('candidates', 'electoral_results.candidate_id', '=', 'candidates.id')
                        ->select('candidates.first_name', 'candidates.last_name', 'votes_count') 
                        ->where('voting_process_id', 1)                                                                     
                        ->where('votes_count',"=", $max_votes)
                        ->get();   */

        /*$GLOBALS["candidatesVotingProcess"] = null;
        Voting_process::find(2)->candidates_voting_processes->each(function ($reg){
            $GLOBALS["candidatesVotingProcess"] = $reg;
            $proposals = Electoral_proposal::where("candidate_id", $reg->id)->each(function ($reg2){
                print_r($reg2);
                $GLOBALS["candidatesVotingProcess"]->proposals = $reg2->proposals;
            });                
        });
        print_r($GLOBALS["candidatesVotingProcess"]);                */
        //$electoral_result = Electoral_result::getElectoralResults(1);
        //print_r($electoral_result);
        //return (new ElectoralResultsExport(1))->download('test2.xlsx');       
        //return Excel::download(new UsersExport, 'users.xlsx');
        /*$voting_process = Voting_process::all()->whereIn('status', [0,4])->each(function($reg){            
            $currentDate = strtotime(date("Y-m-d H:i:s", time()));
            $startDate = strtotime($reg->start_date);            
            //validate if vote is in date range 
            if ($currentDate >= $startDate ) {      
                //Log::info("proceso iniciado");
                echo "proceso iniciado";
            }
            else{
                //Log::info("proceso no iniciado");
                echo "proceso no iniciado";
            }
        });
        //print_r($voting_process);
        */

        /* 
        $data = new stdClass();
        $data->voting_process = "Representante estudiantil udenar";
        $data->identification = "12345678";
        $data->first_name = "Daniel Esteban";
        $data->last_name = "Burbano Salas";
        $data->date = date("Y-m-d H:i:s");
        $data->certificateNumber = "123";

        $data = json_decode(json_encode($data),true);

        $votingCertificate = \PDF::loadView('voting_certificates.voting_certificates', compact('data'));
        //Storage::put('voting_certificates/ejemplo.pdf', $votingCertificate->output());        
        return $votingCertificate->stream('ejemplo.pdf');  */

        /*
        //get count results
        $votesByCandidate = DB::table('candidates_voting_processes')
            ->select(DB::raw('candidates_voting_processes.voting_process_id, candidates_voting_processes.candidate_id, count(electoral_votes.candidate_id) as votes_count'))
            ->leftJoin('electoral_votes', 'electoral_votes.candidate_id', '=', 'candidates_voting_processes.candidate_id')
            ->where('candidates_voting_processes.voting_process_id', 8)
            ->groupBy('candidates_voting_processes.voting_process_id')
            ->groupBy('candidates_voting_processes.candidate_id')
            ->orderBy('votes_count', 'desc')
            ->get();


        //$electoralResults
        print_r($votesByCandidate);*/

        /*$localIP = getHostByName(getHostName());
        print_r($localIP);*/

        /*  $voterVotingProcess = Voter_voting_process::where('voter_id', 1)
            ->where('voting_process_id', 1)
            ->delete();

        print_r($voterVotingProcess); */

        //echo asset('storage/candidates/1_1111118.jpg');

        /* $role = Role::find(2);
        if ($role) {
            $role->modules->each(function ($module) {
                if ($module) {
                    echo $module->endpoint;
                    $uri = "http://localhost/evoting/public/api/user/123";
                    if (strpos($uri, $module->endpoint)) {
                        $module->grants_permissions->each(function ($grant_permission) {
                            print_r($grant_permission->grant_type);
                        });
                    } else {
                        echo 'no';
                    }
                }
            });
        }
 */
      /*   $votingProcess = Voting_process::find(4);
        if (is_object($votingProcess)) {
            $dataMail['voting_process'] = $votingProcess->name;
            $dataMail['start_date'] = $votingProcess->start_date;
            $dataMail['end_date'] = $votingProcess->end_date;
            $GLOBALS['paramsMail'] = array("json" => json_encode($dataMail));
            $votingProcess->voters_voting_processes->each(function ($reg, $key) {
                $GLOBALS["email"] = $reg->email;
                $GLOBALS["sendFirst"] = $key;
                //send email to user
                Mail::send('emails.invitation_to_vote', $GLOBALS['paramsMail'], function ($message) {
                    $message->subject('Invitación proceso de votación');
                    if ($GLOBALS["sendFirst"] == 0) {
                        //$message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->cc(['danielburbano19@gmail.com']);
                    }
                    $message->to($GLOBALS["email"]);
                });
            });
        } */


        $candidate = Candidate::find(1);
        $electoralProposals = $candidate->electoral_proposal;

        print_r($electoralProposals);

    }
}
