<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Electoral_proposal;
use Illuminate\Support\Facades\Validator;

class ElectoralProposalsController extends Controller
{
    public function index()
    {
        $electoralProposal = Electoral_proposal::all();
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'electoralProposal' => json_decode($electoralProposal)
        ]);
    }

    public function show($id)
    {
        $electoralProposal = Electoral_proposal::find($id);
        if (is_object($electoralProposal)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'electoralProposal' => json_decode($electoralProposal),
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información de las propuestas no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'candidate_id' => 'required',
                'proposals' => 'required',
            ]);

            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado las pruestas para el candidato',
                    'errors' => $validate->errors(),
                );
            } else {

                //create proposals to electoralProposal
                $electoralProposals = new Electoral_proposal();
                $electoralProposals->candidate_id = $params['candidate_id'];
                $electoralProposals->proposals = json_encode($params['proposals']);
                $electoralProposals->save();

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'electoral_proposals' => $electoralProposals
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información de las propuestas correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'candidate_id' => 'required',
                'proposals' => 'required',
            ]);
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar la información de las propuestas',
                    'errors' => $validate->errors(),
                );
            } else {
                //get register            
                $electoralProposal = Electoral_proposal::find($id);

                //check if register exist
                if (is_object($electoralProposal)) {
                    //unset data to not update
                    unset($params['id']);
                    unset($params['created_at']);

                    //update register
                    $electoralProposal = Electoral_proposal::where('id', $id)
                                        ->update(
                                            ['candidate_id' => $params['candidate_id'],
                                             'proposals' => json_encode($params['proposals'])]
                                            );

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'electoralProposal' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la información de las propuestas no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información de las propuestas correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $electoralProposal = Electoral_proposal::find($id);

        //check if register exist
        if (!empty($electoralProposal)) {            
            $electoralProposal->delete();
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'electoralProposal' => $electoralProposal,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'La información de las propuestas no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }
}
