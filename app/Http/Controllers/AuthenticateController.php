<?php

namespace App\Http\Controllers;

use App\Voting_process;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class AuthenticateController extends Controller
{
    public function index($processId)
    {
        return view('authentication.face_api', ['processId' => $processId]);
    }

    public function sendNotification(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'ip_address' => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado notificar',
                    'errors' => $validate->errors(),
                );
            } else {
                $votingProcess = Voting_process::find($params['voting_process_id']);
                $dataMail['voting_process'] = $votingProcess->name;
                $dataMail['ip'] = $params['ip_address'];
                $GLOBALS['paramsMail'] = array("json" => json_encode($dataMail));
                //send email to user
                Mail::send('emails.no_authenticated', $GLOBALS['paramsMail'], function ($message) {
                    $message->subject('Invitación proceso de votación');
                    $message->cc(['franciscozambrano@outlook.es']);
                    $message->to('danielburbano19@gmail.com');
                });
                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'params' => $params,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el rol correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
