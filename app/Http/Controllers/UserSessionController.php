<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User_session;
use App\Helpers\JwtAuth;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Validator;

class UserSessionController extends Controller
{
    public function index()
    {
        $user_session = User_session::all();

        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'user_session' => $user_session
        ]);
    }

    public function show($id)
    {
        $user_session = User_session::find($id);
        if (is_object($user_session)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'user_session' => $user_session,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la sesión de usuario no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function update(Request $request)
    {
        $token = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $user_decoded = $jwtAuth->checkToken($token, true);
        if ($user_decoded) {
            $userId = $user_decoded->sub;
            $user_session = User_session::where('user_id', $userId)->get()->last();
            //update register        
            if (is_object($user_session)) {
                $params["end_date"] = date('Y-m-d H:i:s');
                $user_session = User_session::where('id', $user_session->id)->update($params);
                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'user_session' => $params,
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'Error la sesión de usuario no existe',
                );
            }
        } else {
            $data = array(
                'status' => 'Success',
                'code' => '200',                
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
