<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Electoral_vote;
use App\Voter;
use App\Voter_voting_process;
use App\Voting_process;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Cast\Unset_;
use PDF;
use stdClass;

class ElectoralVoteController extends Controller
{
    
    public function show($id)
    {
        $voter_voting_process = Voter_voting_process::find($id);

        if (is_object($voter_voting_process)) {
            if (isset($voter_voting_process->vote_date)) {                
                $voter = Voter::find($voter_voting_process->voter_id);
                $votingProcess = Voting_process::find($voter_voting_process->voting_process_id);
                $certificateVoter = new stdClass();
                $certificateVoter->voting_process = $votingProcess->name;
                $certificateVoter->identification = $voter->identification;
                $certificateVoter->first_name = $voter->first_name;
                $certificateVoter->last_name = $voter->last_name;
                $certificateVoter->vote_date = $voter_voting_process->vote_date;
                $certificateVoter->certificateNumber = $voter_voting_process->id;

                $data = array(
                    'code' => '200',
                    'status' => 'Success',
                    'certificate' => $certificateVoter,
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'Error no existe información asociada al código QR',
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error no existe información asociada al código QR',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'voting_process_id' => 'required',
                'candidate_id' => 'required',
                'voter_id' => 'required',
                'ip_address' => 'required',
            ]);
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear el voto',
                    'errors' => $validate->errors(),
                );
            } else {

                $votingProcess = Voting_process::find($params['voting_process_id']);
                //validate if voting process is initiated
                if ($votingProcess->status == "1") {
                    $currentDate = strtotime(date("Y-m-d H:i:s", time()));
                    $startDate = strtotime($votingProcess->start_date);
                    $endDate = strtotime($votingProcess->end_date);
                    //validate if vote is in date range 
                    if ($currentDate >= $startDate && $currentDate <= $endDate) {
                        $voter_voting_process = Voter_voting_process::where('voter_id', $params['voter_id'])
                            ->where('voting_process_id', $params['voting_process_id'])
                            ->first();
                        //vadate if voter is already registered your vote
                        if ($voter_voting_process->registred_vote == "0") {
                            //correct validation  

                            //save vote
                            $electoral_vote = new Electoral_vote();
                            $electoral_vote->voting_process_id = $params['voting_process_id'];
                            $electoral_vote->candidate_id = $params['candidate_id'];
                            $electoral_vote->ip_address = $params['ip_address'];
                            $electoral_vote->save();

                            //unset params to response
                            unset($electoral_vote->candidate_id);
                            unset($electoral_vote->id);

                            $currentDateReg = date("Y-m-d H:i:s");

                            //save voter register 
                            $voter_voting_process->registred_vote = "1";
                            $voter_voting_process->vote_date = $currentDateReg;
                            $voter_voting_process->save();


                            //install dompdf
                            //composer require barryvdh/laravel-dompdf
                            //php artisan vendor:publish

                            //generate voting certificate
                            $voter = Voter::find($params['voter_id']);
                            $data = new stdClass();
                            $data->voting_process = $votingProcess->name;
                            $data->identification = $voter->identification;
                            $data->first_name = $voter->first_name;
                            $data->last_name = $voter->last_name;
                            $data->date = $currentDateReg;
                            $data->certificateNumber = $voter_voting_process->id;
                            $data = json_decode(json_encode($data), true);
                            $GLOBALS["certificateName"] = $votingProcess->id . '_' . $voter->identification . '.pdf';
                            $GLOBALS["email"] = $voter->email;

                            $votingCertificate = PDF::loadView('voting_certificates/voting_certificates', compact('data'));
                            Storage::put('voting_certificates/' . $GLOBALS["certificateName"], $votingCertificate->output());

                            $GLOBALS["votingProcessname"] = $votingProcess->name;
                            $paramsMail = array("json" => json_encode($params));
                            Mail::send('voting_certificates.voting_certificates', compact('data'), function ($message) {
                                $message->subject('Certificado de votación ' . $GLOBALS["votingProcessname"]);
                                $message->cc(['franciscozambrano@outlook.es','danielburbano19@gmail.com']);
                                $message->to($GLOBALS["email"]);
                                $message->attach('http://localhost/evoting/storage/app/voting_certificates/' . $GLOBALS["certificateName"]);
                            });

                            $data = array(
                                'status' => 'Success',
                                'code' => '200',
                                'electoral_vote' => $electoral_vote,
                                'vonting_certificate' => asset('storage/voting_certificates/'. $votingProcess->id . '_' . $voter->identification . '.pdf')//'http://localhost/evoting/storage/app/voting_certificates/' . $votingProcess->id . '_' . $voter->identification . '.pdf'
                            );
                        } else {
                            $data = array(
                                'status' => 'Error',
                                'code' => '404',
                                'message' => 'Error el votante ya ha registrado su voto',
                            );
                        }
                    } else {
                        $data = array(
                            'status' => 'Error',
                            'code' => '404',
                            'message' => 'Error no se puede registar el voto, la fecha actual es diferente al rango configurado para el proceso de votación',
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error el proceso de votación tiene un estado diferente a iniciado',
                    );
                }

                //generate vote certificate
                /*$votingProcessName = Voting_process::find($params['voting_process_id'])->name;
                $voterFirstName = Voting_process::find($params['voter_id'])->first_name;
                $voterLastName = Voting_process::find($params['voter_id'])->last_name;*/
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del voto correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
