<?php

namespace App\Http\Controllers;

use App\Candidate;
use App\Imports\votersImport;
use App\User_importer;
use Illuminate\Http\Request;
use App\Voting_process;
use App\Candidate_voting_process;
use App\Electoral_result;
use App\Voter_voting_process;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Voter;
use App\Electoral_proposal;
use App\Exports\ElectoralResultsExport;
use App\Jobs\SendNotificationVoters;
use App\Jobs\StartFinishVotingProcesses;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use stdClass;
use PDF;

class VotingProcessController extends Controller
{
    public function index()
    {
        //get all registers        
        $voting_process = Voting_process::all()->each(function ($reg) {
            //validate if VotingProcess have candidates
            $GLOBALS["candidatesNumber"] = 0;
            Candidate_voting_process::where("voting_process_id", $reg->id)->each(function ($reg2) {
                if ($reg2->id) {
                    $GLOBALS["candidatesNumber"]++;
                }
            });
            $reg->candidates_number = $GLOBALS["candidatesNumber"];
        });
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'voting_process' => $voting_process
        ]);
    }

    public function show($id)
    {
        $voting_process = Voting_process::find($id);
        if (is_object($voting_process)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voting_process' => $voting_process,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del proceso de votación no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $file_users = $request->file("file_users");
        //$file_candidates = $request->file_candidates("file_candidates");
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);

        //get voting process image if exist
        $votingProcessImage = $request->file('file0');

        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'name' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'user_id' => 'required',
                //'import_type' => 'required'
            ]);

            if ($votingProcessImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            $validateFile = Validator::make($request->all(), [
                'file_users' => 'required|mimes:xlsx'
            ]);

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado el proceso de votación',
                    'errors' => $validate->errors(),
                );
            } elseif ($validateFile->fails()) {
                //file_users with errors in validation
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado el proceso de votación error archivo xlsx',
                    'errors' => $validate->errors(),
                );
            } else {
                //save data
                //correct validation            
                $voting_process = new Voting_process();
                $voting_process->name = $params['name'];
                $voting_process->description = $params['description'];
                $voting_process->start_date = $params['start_date'];
                $voting_process->end_date = $params['end_date'];
                $voting_process->user_id = $params['user_id'];
                $voting_process->save();

                //import voters from file_users
                $votersImport = new votersImport;
                $votersImport->setVotingProcessInfo($voting_process->id);
                Excel::import($votersImport, $file_users);
                $resultVotersImport = $votersImport->getImportResult();

                if ($resultVotersImport->importResult !== false) {
                    //create User_importer                
                    $votersImporter = new User_importer();
                    //$votersImporter->import_type = $params['import_type'];
                    $votersImporter->file_name =  $request->file("file_users")->getClientOriginalName();
                    $votersImporter->upload_date = date('Y-m-d H:i:s');
                    $votersImporter->user_id = $params['user_id'];
                    $votersImporter->voting_process_id = $voting_process->id;
                    $votersImporter->save();

                    if ($votingProcessImage) {
                        $imageName = $voting_process->id . ".jpg";
                        //save image
                        Storage::disk('voting_processes')->put($imageName, File::get($votingProcessImage));
                        $voting_process->image = $imageName;
                    }

                    //create blank vote
                    $blankVoteCandidate = new Candidate();
                    $blankVoteCandidate->identification = "-";
                    $blankVoteCandidate->first_name = "Voto en blanco";
                    $blankVoteCandidate->last_name = "-";
                    $blankVoteCandidate->birthdate = date('Y-m-d H:i:s');
                    $blankVoteCandidate->gender = "-";
                    $blankVoteCandidate->email = "-";
                    $blankVoteCandidate->save();

                    $blankVoteProposal = new Electoral_proposal();
                    $blankVoteProposal->candidate_id = $blankVoteCandidate->id;
                    $blankVoteProposal->proposals = json_encode(array());
                    $blankVoteProposal->save();

                    $blankVoteVotingProcess = new Candidate_voting_process();
                    $blankVoteVotingProcess->voting_process_id = $voting_process->id;
                    $blankVoteVotingProcess->candidate_id = $blankVoteCandidate->id;
                    $blankVoteVotingProcess->save();

                    //create jobs to init voting process                    
                    $this->setJobs();
                   
                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'voting_process' => $voting_process,
                    );
                } else {
                    //delete voting process
                    $voting_process->delete();

                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se ha cargado la imagen para el votante con identificacion: ' . $resultVotersImport->notSetImage,
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del proceso de votación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function update($id, Request $request)
    {
        //get data from post request    
        $file_users = $request->file("file_users");
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        //get voting process image if exist
        $votingProcessImage = $request->file('file0');

        if (!empty($params)) {

            //get register
            $voting_process = Voting_process::find($id);

            $validate = Validator::make($params, [
                'name' => 'required',
                'description' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'user_id' => 'required',
            ]);

            if ($votingProcessImage) {
                if (!$validate->fails()) {
                    $validate = Validator::make($request->all(), [
                        'file0' => 'required|mimes:jpg,jpeg'
                    ]);
                }
            }

            $validateFile = false;
            if ($file_users) {
                $validatorFile = Validator::make($request->all(), [
                    'file_users' => 'mimes:xlsx'
                ]);
                $validateFile = $validatorFile->fails();
            }


            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar la información del proceso de votación',
                    'errors' => $validate->errors(),
                );
            } elseif ($validateFile) {
                //file_users with errors in validation
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha creado el proceso de votación',
                    'errors' => $validate->errors(),
                );
            } else {

                //unset data to not update
                unset($params['id']);
                unset($params['created_at']);

                if (is_object($voting_process)) {
                    //update register
                    if ($voting_process->status == "0" || $voting_process->status == "4") {
                        Voting_process::where('id', $id)->update($params);

                        //update voters list if isset
                        if (isset($file_users)) {

                            //delete voters
                            //$voting_process->voters_voting_processes->each->delete();

                            //delete voters of voting process and pivot table                   
                            //$voting_process->voters_voting_processes()->detach(); //don´t work with soft delete                                               
                            /*Voter_voting_process::where("voting_process_id", $id)->get()->each(function ($reg) {
                            $reg->update(array('deleted_at' => date('Y-m-d H:i:s')));
                        });*/

                            //import voters from file_users
                            $votersImport = new votersImport;
                            $votersImport->setVotingProcessInfo($id);
                            Excel::import($votersImport, $file_users);

                            //create User_importer                
                            $votersImporter = new User_importer();
                            //$votersImporter->import_type = $params['import_type'];
                            $votersImporter->file_name =  $request->file("file_users")->getClientOriginalName();
                            $votersImporter->upload_date = date('Y-m-d H:i:s');
                            $votersImporter->user_id = $params['user_id'];
                            $votersImporter->voting_process_id = $voting_process->id;
                            $votersImporter->save();
                        }

                        //update image if exist
                        if ($votingProcessImage) {
                            $imageName = $voting_process->id . ".jpg";
                            //save image
                            Storage::disk('voting_processes')->put($imageName, File::get($votingProcessImage));
                            $params['image'] = $imageName;
                        }

                        //update jobs
                        $this->setJobs();

                        $data = array(
                            'status' => 'Success',
                            'code' => '200',
                            'voting_process' => $params,
                        );
                    } else {
                        $data = array(
                            'status' => 'Error',
                            'code' => '404',
                            'message' => 'Error no se puede actualizar un proceso de votación en estado iniciado, cancelado o finalizado',
                        );
                    }
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la información del proceso de votación no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la información del proceso de votación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }


    /*
    status: 0 = creado
            1 = iniciado
            2 = finalizado
            3 = cancelado
            4 = suspendido
    */
    public function changeStatus(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {

            //validate data
            $validate = Validator::make($params, [
                'user_id' => 'required',
                'voting_process_id' => 'required',
                'status' => 'required',
            ]);

            $votingProcess = Voting_process::find($params['voting_process_id']);
            if (is_object($votingProcess)) {
                if ($validate->fails()) {
                    //json with errors in validation data
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se ha logrado actualizar el rol',
                        'errors' => $validate->errors(),
                    );
                } elseif ($votingProcess->status == "1" && $params['status'] == "0") {
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se ha logrado actualizar el estado del proceso de votación por que ya ha sido iniciado solo puede ser finalizado, cancelado o suspendido'
                    );
                } elseif ($votingProcess->status == "2") {
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se ha logrado actualizar el estado del proceso de votación por que ya ha sido finalizado'
                    );
                } elseif ($votingProcess->status == "3") {
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'No se ha logrado actualizar el estado del proceso de votación por que ha sido cancelado'
                    );
                } else {

                    //update register
                    $votingProcess->status = $params['status'];
                    $votingProcess->user_id = $params['user_id'];
                    $votingProcess->save();

                    //validate process finished
                    if ($params['status'] == "2") {
                        Electoral_result::store($params['voting_process_id']);
                    } elseif ($params['status'] == "3") {
                    }

                    //update jobs
                    $this->setJobs();

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'role' => $params,
                    );
                }
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'Error proceso de votación no existe',
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el json de cambio de estado del proceso de votación correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function destroy($id, Request $request)
    {
        //get register
        $voting_process = Voting_process::find($id);

        //check if register exist
        if (!empty($voting_process)) {

            if ($voting_process->status == "1") {
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado eliminar el proceso de votación por que ya ha sido iniciado'
                );
            } else {
                $voting_process->delete();

                //update jobs
                $this->setJobs();

                $data = array(
                    'status' => 'Success',
                    'code' => '200',
                    'voting_process' => $voting_process,
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'La información del proceso de votación no existe',
            );
        }

        //devolver el resultado
        return response()->json($data, $data['code']);
    }

    public function candidates($id)
    {
        $GLOBALS["candidatesVotingProcess"] = array();
        if (is_object(Voting_process::find($id))) {
            Voting_process::find($id)->candidates_voting_processes->each(function ($reg, $key) {
                array_push($GLOBALS["candidatesVotingProcess"], $reg);
                $proposals = Electoral_proposal::where("candidate_id", $reg->id)->get();
                foreach ($proposals as $proposal) {
                    $GLOBALS["candidatesVotingProcess"][$key]->proposals = json_decode($proposal->proposals);
                }
            });
            if (is_array($GLOBALS["candidatesVotingProcess"])) {

                $votingProcess = Voting_process::find($id);
                //unset blank vote from candidates 
                $realCandidates['candidates'] = $GLOBALS["candidatesVotingProcess"];
                $realCandidates['voting_process'] = $votingProcess->name;
                unset($realCandidates['candidates'][0]);
                //voting_process_candidates
                $fileName = 'candidates_process_' . $id . '.pdf';

                $votingResults = PDF::loadView('candidates_list/candidates_list', compact('realCandidates'));
                Storage::put('voting_process_candidates/' . $fileName, $votingResults->output());

                $data = array(
                    'code' => '200',
                    'status' => 'Success',
                    'voting_process_candidates' => $GLOBALS["candidatesVotingProcess"],
                    'candidates_pdf' => asset('storage/voting_process_candidates/' . $fileName) //'http://localhost/evoting/storage/app/voting_process_candidates/' . $fileName
                );
            } else {
                $data = array(
                    'status' => 'Error',
                    'code' => '404',
                    'message' => 'Error la información del proceso de votación no existe',
                );
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del proceso de votación no existe',
            );
        }

        return response()->json($data, $data['code']);
    }

    public function voters($id)
    {
        $votersVotingProcess = Voting_process::find($id)->voters_voting_processes;
        if (is_object($votersVotingProcess)) {
            $votingProcess = Voting_process::find($id);
            $votersList['voters'] = $votersVotingProcess;
            $votersList['voting_process'] = $votingProcess->name;

            $fileName = 'voters_process_' . $id . '.pdf';
            $votingResults = PDF::loadView('voters_list/voters_list', compact('votersList'));
            Storage::put('voting_process_voters/' . $fileName, $votingResults->output());

            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voting_process_voters' => $votersVotingProcess,
                'voters_pdf' => asset('storage/voting_process_voters/' . $fileName)
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del proceso de votación no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function votersLabels($id)
    {
        $GLOBALS["labels"] = array();
        Voting_process::find($id)->voters_voting_processes->each(function ($reg) {
            array_push($GLOBALS["labels"], $reg->identification);
        });
        if ($GLOBALS["labels"]) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voters_labels' => $GLOBALS["labels"],
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la información del proceso de votación no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function getImage($fileName)
    {
        $isset = Storage::disk('voting_processes')->exists($fileName . ".jpg");
        if ($isset) {
            $file = Storage::disk('voting_processes')->get($fileName . ".jpg");
            return new Response($file, 200);
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Imagen no encontrada',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function getResultsByProcess($id)
    {

        //get total votes by voting process
        $GLOBALS["totalVotes"] = Electoral_result::getTotalVotes($id);

        //get candidates number by voting process
        $candidatesNumber = Electoral_result::getCandidatesNumber($id);

        //get voters number by voting process
        $votersNumber = count(Voting_process::find($id)->voters_voting_processes);

        //number abstentionism voters
        $abstentionismVoters = $votersNumber - $GLOBALS["totalVotes"];

        //porcentage abstentionism voters
        $abstentionismVotersPorcentage = 0;
        if ($votersNumber) {
            $abstentionismVotersPorcentage = round(($abstentionismVoters * 100) / $votersNumber, 2);
        }

        //porcentage active voters
        $activeVotersPorcentage = round(100 - $abstentionismVotersPorcentage, 2);

        //blank votes
        $GLOBALS['blankVotes'] = 0;
        $GLOBALS['blankVotesPorcentage'] = 0;

        //get results gruop by candidate
        $votesByCandidate = Electoral_result::getElectoralResults($id)->each(function ($reg, $key) {
            //get votes and porcentage blank votes
            if ($reg->first_name === "Voto en blanco") {
                $reg->votes_porcentage = 0;
                $GLOBALS['blankVotes'] = $reg->votes_count;
                if ($GLOBALS["totalVotes"]) {
                    $GLOBALS['blankVotesPorcentage'] = round(($reg->votes_count * 100) / $GLOBALS["totalVotes"], 2);
                    $reg->votes_porcentage = $GLOBALS['blankVotesPorcentage'];
                }
            } else {
                $reg->votes_porcentage = 0;
                if ($GLOBALS["totalVotes"]) {
                    $reg->votes_porcentage = round(($reg->votes_count * 100) / $GLOBALS["totalVotes"], 2);
                }
            }
        });

        if ($votesByCandidate->isEmpty()) {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'No se encontraron registros para el proceso de votación: ' . $id,
            );
        } else {
            (new ElectoralResultsExport($id))->store($id . '.xlsx', 'voting_processes_results');

            $votingProcessName = Voting_process::find($id)->name;
            $resultsData = new stdClass();
            $resultsData->voting_process = $votingProcessName;
            $resultsData->total_votes = $GLOBALS['totalVotes'];
            $resultsData->candidates_number = $candidatesNumber;
            $resultsData->voters_number = $votersNumber;
            $resultsData->active_voters_porcentage = $activeVotersPorcentage;
            $resultsData->abstentionism_voters_porcentage = $abstentionismVotersPorcentage;
            $resultsData->blank_votes = $GLOBALS['blankVotes'];
            $resultsData->blank_votes_porcentage = $GLOBALS['blankVotesPorcentage'];
            $resultsData->results_by_candidate = $votesByCandidate;
            $resultsData = json_decode(json_encode($resultsData), true);
            $fileName = $id . '.pdf';

            $votingResults = PDF::loadView('electoral_results/electoral_results', compact('resultsData'));
            Storage::put('voting_processes_results/' . $fileName, $votingResults->output());

            $data = array(
                'status' => 'Success',
                'code' => '200',
                'total_votes' => $GLOBALS['totalVotes'],
                'candidates_number' => $candidatesNumber - 1,
                'voters_number' => $votersNumber,
                'active_voters_porcentage' => $activeVotersPorcentage,
                'abstentionism_voters_porcentage' => $abstentionismVotersPorcentage,
                'blank_votes' => $GLOBALS['blankVotes'],
                'blank_votes_porcentage' => $GLOBALS['blankVotesPorcentage'],
                'results_by_candidate' => $votesByCandidate,
                'url_report' => asset('storage/voting_processes_results/' . $fileName) //'http://localhost/evoting/storage/app/voting_processes_results/' . $fileName
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function getWinnerByProcess($id)
    {

        $winner = Electoral_result::getWinner($id);

        if ($winner->isEmpty()) {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'No se encontraron registros para el proceso de votación: ' . $id,
            );
        } else {
            $data = array(
                'status' => 'Success',
                'code' => '200',
                'results_winner' => $winner,
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public static function setJobs()
    {
        //delete all jobs to create news
        DB::table('jobs')->delete();

        //create job to start and finish voting process
        Voting_process::all()->whereIn('status', [0, 1])->each(function ($reg) {
            //create job to start voting process          
            $job = new StartFinishVotingProcesses($reg->id, 1);
            $startDate = Carbon::createFromFormat('Y-m-d H:i:s', $reg->start_date);
            dispatch($job)->delay($startDate);

            //create job to finish voting process          
            $job = new StartFinishVotingProcesses($reg->id, 2);
            $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $reg->end_date);
            dispatch($job)->delay($endDate);
        });

        //create job to send email notifications to voters 
        Voting_process::all()->where('sendNotification', '0')->each(function ($reg) {                     
             $job = new SendNotificationVoters($reg->id);
             $sendDate = Carbon::createFromFormat('Y-m-d H:i:s', $reg->start_date);
             //subtract one day
             $sendDate->subDay();
             dispatch($job)->delay($sendDate);
        });
    }

    public function sendInviteEmailsByproccess($id)
    {
        $votingProcess = Voting_process::find($id);
        if (is_object($votingProcess)) {
            $dataMail['voting_process'] = $votingProcess->name;
            $dataMail['start_date'] = $votingProcess->start_date;
            $dataMail['end_date'] = $votingProcess->end_date;
            $GLOBALS['paramsMail'] = array("json" => json_encode($dataMail));
            $votingProcess->voters_voting_processes->each(function ($reg, $key) {
                $GLOBALS["email"] = $reg->email;
                $GLOBALS["sendFirst"] = $key;
                //send email to user
                Mail::send('emails.invitation_to_vote', $GLOBALS['paramsMail'], function ($message) {
                    $message->subject('Invitación proceso de votación');
                    if ($GLOBALS["sendFirst"] == 0) {
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                    }
                    $message->to($GLOBALS["email"]);
                });
            });

            $data = array(
                'status' => 'Success',
                'code' => '200'
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'No se encontraron registros para el proceso de votación: ' . $id,
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
