<?php

namespace App\Http\Controllers;

use App\Voter_request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\VoterRequest;
use App\Voting_process;
use stdClass;

class VoterRequestController extends Controller
{
    public function index()
    {
        $voterRequest = Voter_request::all();

        if (is_object($voterRequest)) {
            $voterRequest->each(function ($reg) {
                $votingProcess = Voting_process::find($reg->voting_process_id);
                $reg->voting_process_name = $votingProcess->name;
            });
        }
        return response()->json([
            'code' => 200,
            'status' => 'Success',
            'voterRequest' => $voterRequest
        ]);
    }

    public function show($id)
    {
        $voterRequest = Voter_request::find($id);
        if (is_object($voterRequest)) {
            $votingProcess = Voting_process::find($voterRequest->voting_process_id);
            $voterRequest->voting_process_name = $votingProcess->name;
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voterRequest' => $voterRequest,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la solicitud no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function store(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null); //if input is empty, assign null value to json
        $params = json_decode($json, true);
        if (!empty($params)) {
            //validate data
            $validate = Validator::make($params, [
                'identification' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|email',
                "voting_process_id" => 'required',
            ]);
            //save data
            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado crear la novedad para ser enviada con el area correspondiente',
                    'errors' => $validate->errors(),
                );
            } else {
                //correct validation
                $voterRequest = new Voter_request();
                $voterRequest->identification = $params['identification'];
                $voterRequest->first_name = $params['first_name'];
                $voterRequest->last_name = $params['last_name'];
                $voterRequest->email = $params['email'];
                $GLOBALS["email"] = $params['email'];

                if (isset($params['comments'])) {
                    $voterRequest->comments = $params['comments'];
                }
                $voterRequest->voting_process_id = $params['voting_process_id'];
                $votingProcess = Voting_process::find($params['voting_process_id']);
                if (is_object($votingProcess)) {
                    $voterRequest->save();
                    $params['voting_process_name'] = $votingProcess->name;
                    $params['voter_request_id'] = $voterRequest->id;
                    $paramsMail = array("json" => json_encode($params));

                    //send email to admin
                    Mail::send('emails.voters_request', $paramsMail, function ($message) {
                        $message->subject('Solicitud de novedad votante');
                        $message->cc(['franciscozambrano@outlook.es']);
                        $message->to('danielburbano19@gmail.com');
                    });

                    //send email to user
                    Mail::send('emails.voters_request_voter', $paramsMail, function ($message) {
                        $message->subject('Registro solicitud de novedad votante');
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->to($GLOBALS["email"]);
                    });


                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'voterRequest' => $voterRequest,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '400',
                        'message' => 'El ID del proceso de votación enviado no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado la novedad correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }

    public function getVoterRequest($identification)
    {
        $voterRequest = Voter_request::where('identification', $identification)->get();
        if (is_object($voterRequest)) {
            $data = array(
                'code' => '200',
                'status' => 'Success',
                'voterRequest' => $voterRequest,
            );
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '404',
                'message' => 'Error la solicitud no existe',
            );
        }
        return response()->json($data, $data['code']);
    }

    public function reply(Request $request)
    {
        //get data from post request
        $json = $request->input('json', null);
        $params = json_decode($json, true);

        if (!empty($params)) {

            //validate data
            $validate = Validator::make($params, [
                'voter_request_id' => 'required',
                'status' => 'required|integer|between:1,2'
            ]);

            if ($validate->fails()) {
                //json with errors in validation data
                $data = array(
                    'status' => 'Error',
                    'code' => '400',
                    'message' => 'No se ha logrado actualizar el estado de la novedad reportada',
                    'errors' => $validate->errors(),
                );
            } else {

                //update fields
                $voterRequest = Voter_request::find($params['voter_request_id']);
                if (is_object($voterRequest)) {
                    $voterRequest->status = $params['status'];
                    $GLOBALS["email"] = $voterRequest->email;

                    $votingProcess = Voting_process::find($voterRequest->voting_process_id);
                    $data = new stdClass();
                    $data->voter_request_id = $params['voter_request_id'];
                    $data->voting_process = $votingProcess->name;
                    $data->first_name = $voterRequest->first_name;
                    $data->last_name = $voterRequest->last_name;
                    $data->status = $params['status'];
                    if (isset($params['reply'])) {
                        $data->reply = $params['reply'];
                        $voterRequest->reply = $params['reply'];
                    }
                    $paramsMail = array("json" => json_encode($data));

                    $voterRequest->save();

                    Mail::send('emails.voters_reply', $paramsMail, function ($message) {
                        $message->subject('Respuesta a la novedad Vote - APP');
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);
                        $message->to($GLOBALS["email"]);
                    });

                    $data = array(
                        'status' => 'Success',
                        'code' => '200',
                        'voterRequest' => $params,
                    );
                } else {
                    $data = array(
                        'status' => 'Error',
                        'code' => '404',
                        'message' => 'Error la novedad que se intenta responder no existe',
                    );
                }
            }
        } else {
            $data = array(
                'status' => 'Error',
                'code' => '400',
                'message' => 'No se ha enviado el rol correctamente',
            );
        }
        //return response
        return response()->json($data, $data['code']);
    }
}
