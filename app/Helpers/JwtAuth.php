<?php

namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Voter;
use App\Role;

class JwtAuth
{
    private $key;

    public function __construct()
    {
        $this->key = "key_secret_evoting-2020";
    }

    public function signup($user_name, $password, $getToken = null)
    {
        //search user credentials user_name and password in data base
        $user = User::where([
            'user_name' => $user_name,
            'password' => $password
        ])->first();

        //check if user credential are valids
        $signup = false;
        if (is_object($user)) {
            $signup = true;
        }
        //Create token with user data and expires token date 
        if ($signup) {
            $token = array(
                'sub' => $user->id,
                'user_name' => $user->user_name,
                'role' => $user->role_id,
                'iat' => time(),
                'exp' => time() + (60 * 60 * 24 * 7) //token expires in 1 hour
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);

            if (is_null($getToken)) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }
        } else {
            $data = false;
        }

        return $data;
    }

    public function signupVoter($identification, $getToken = null)
    {
        //search user credentials user_name and password in data base
        $voter = Voter::where([
            'identification' => $identification,
        ])->first();

        //check if user credential are valids
        $signup = false;
        if (is_object($voter)) {
            $signup = true;
        }
        //Create token with user data and expires token date 
        if ($signup) {
            $token = array(
                'sub' => $voter->id,
                'first_name' => $voter->first_name,
                'last_name' => $voter->last_name,
                'iat' => time(),
                'exp' => time() + (60 * 60 * 24 * 7) //token expires in 1 hour
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);

            if (is_null($getToken)) {
                $data = $jwt;
            } else {
                $data = $decoded;
            }
        } else {
            $data = false;
        }

        return $data;
    }

    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        } catch (\Firebase\JWT\ExpiredException $e) {
            $auth = false;
        }

        if (!empty($decoded) && is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }

        if ($getIdentity) {
            if ($auth) {
                return $decoded;
            }
        }
        return $auth;
    }

    public function checkRole($jwt, $uri, $grantType)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, ['HS256']);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        } catch (\Firebase\JWT\ExpiredException $e) {
            $auth = false;
        }

        if (is_object($decoded) && isset($decoded->sub)) {
            //validate if exist role
            $role = Role::find($decoded->role);
            if ($role) {
                foreach ($role->modules as $module) {
                    //validate if exist module
                    if ($module) {
                        //validate if uri exist in modules in role
                        if (strpos($uri, $module->endpoint)) {
                            $flagUriAllowed = false;
                            $pos = strpos($uri, $module->endpoint);
                            $lenSearch = $pos + strlen($module->endpoint);
                            if ($lenSearch == strlen($uri)) {
                                $flagUriAllowed = true;
                            } else {
                                //validate numeric uri complement
                                $strRest = substr($uri, $lenSearch + 1);
                                if ($strRest && is_numeric($strRest)) {
                                    $flagUriAllowed = true;
                                }
                            }
                            if ($flagUriAllowed) {
                                foreach ($module->grants_permissions as $grant_permission) {
                                    if ($grant_permission) {
                                        if ($grantType == $grant_permission->grant_type) {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //return result
        return $auth;
    }
}
