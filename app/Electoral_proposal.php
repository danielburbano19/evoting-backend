<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Electoral_proposal extends Model
{
    use SoftDeletes;
    
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'electoral_proposals';

    
    //one-to-one inverse
    public function candidate()
    {
        return $this->belongsTo('App\Candidate');
    }
}
