<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ElectoralResultsExport implements FromQuery, WithHeadings
{

    use Exportable;

    private $votingProcessId; 
    
    public function __construct($id)
    {
        $this->votingProcessId = $id;        
    }


    /**
    * @return \Illuminate\Database\Query\Builder
    */
    public function query()
    {
        $results = DB::table('electoral_results')                        
        ->join('candidates', 'electoral_results.candidate_id', '=', 'candidates.id')
        ->select(DB::raw('candidates.first_name, candidates.last_name, electoral_results.votes_count')) 
        ->where('voting_process_id', $this->votingProcessId)
        ->groupBy('candidate_id')
        ->groupBy('candidates.first_name') 
        ->groupBy('candidates.last_name')    
        ->groupBy('electoral_results.votes_count')    
        ->orderBy('electoral_results.votes_count', 'desc');                                           

        return $results;
    }

    public function headings() :array
    {
        return ["Nombres", "Apellidos", "Total votos"];
    }
}
