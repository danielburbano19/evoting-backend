<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Voter;
use App\Voter_voting_process;
use App\Voting_process;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;

class votersImport implements ToCollection
{

    private $votingProcessId;
    private $importResult = true;
    private $notSetImage = null;
    private $arrayVotersId = array();

    public function setVotingProcessInfo(int $id)
    {
        $this->votingProcessId = $id;
    }

    //return if import was successfull
    public function getImportResult()
    {
        $result = new stdClass();
        $result->importResult = $this->importResult;
        $result->notSetImage = $this->notSetImage;

        return $result;
    }

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $identification = $row[0];

            $voter = Voter::where('identification', $identification)->first();

            //validate voter image exist in voting process
            $isset = Storage::disk('voters')->exists($identification . ".jpg");
            if (!$isset) {
                $this->importResult = false;
                $this->notSetImage = $identification;
                $this->deleteRegisters();
                return;      
            }

            if (is_object($voter)) {
                //update existent voter
                $voter->first_name = $row[1];
                $voter->last_name = $row[2];
                $voter->birthdate = $row[3];
                $voter->gender = $row[4];
                $voter->email = $row[5];
                $voter->updated_at = date('Y-m-d H:i:s');
                $voter->save();

                $voter_id = $voter->id;
            } else {
                //create new voter                
                $voter_id = DB::table('voters')->insertGetId([
                    "identification" => $row[0],
                    "first_name" => $row[1],
                    "last_name" => $row[2],
                    "birthdate" => $row[3],
                    "gender" => $row[4],
                    "email" => $row[5],
                    "created_at" => date('Y-m-d H:i:s'),
                    "updated_at" => date('Y-m-d H:i:s'),
                ]);
                array_push($this->arrayVotersId, $voter_id);
            }

            $voterVotingProcess = Voter_voting_process::where('voter_id', $voter_id)
                ->where('voting_process_id', $this->votingProcessId)
                ->get();

            //create new register in Voter_voting_process if not exist                                                        
            if ($voterVotingProcess->isEmpty()) {
                //asociate voter to voting_process
                $voterVotingProcess = new Voter_voting_process();
                $voterVotingProcess->voter_id = $voter_id;
                $voterVotingProcess->voting_process_id = $this->votingProcessId;
                $voterVotingProcess->save();
            }
        }
    }

    private function deleteRegisters()
    {
        foreach ($this->arrayVotersId as $voter_id) {
            //delete Voter_voting_process
            Voter_voting_process::where('voter_id', $voter_id)
                ->where('voting_process_id', $this->votingProcessId)
                ->delete();
        
            //delete Voter
            Voter::find($voter_id)->delete();
        }
    }
}
