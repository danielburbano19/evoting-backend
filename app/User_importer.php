<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_importer extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_importers';

    //one-to-many (inverse) relationships
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    //one-to-many (inverse) relationships
    public function import_type(){
        return $this->belongsTo('App\Import_type','id');
    }

    //one-to-many (inverse) relationships
    public function voting_process(){
        return $this->belongsTo('App\voting_process','voting_process_id');
    }
}
