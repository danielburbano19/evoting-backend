<?php

namespace App\Jobs;

use App\Electoral_result;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Voting_process;
use Illuminate\Support\Facades\Log;

class StartFinishVotingProcesses implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $votingProcessId;
    protected $votingProcessStatus;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($votingProcessId, $votingProcessStatus)
    {
        $this->votingProcessId = $votingProcessId;
        $this->votingProcessStatus = $votingProcessStatus;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $voting_process = Voting_process::find($this->votingProcessId);
        //validate if voting process status is diferent to canceled or paused    
        if ($this->votingProcessStatus == 1 || $this->votingProcessStatus == 2) {            
            $voting_process->status = $this->votingProcessStatus;
        }
        //validate if voting process status is finished, update electoral results table
        if ($this->votingProcessStatus == 2) {            
            Electoral_result::store($this->votingProcessId);
        }
        $voting_process->save();
    }
}
