<?php

namespace App\Jobs;

use App\Voting_process;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendNotificationVoters implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $votingProcessId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($votingProcessId)
    {
        $this->votingProcessId = $votingProcessId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {        
        $votingProcess = Voting_process::find($this->votingProcessId);        
        Log::channel('evoting')->info('Send Mail Notification to voting process: '. $votingProcess->name);
        if (is_object($votingProcess)) {            
            $dataMail['voting_process'] = $votingProcess->name;
            $dataMail['start_date'] = $votingProcess->start_date;
            $dataMail['end_date'] = $votingProcess->end_date;
            $GLOBALS['paramsMail'] = array("json" => json_encode($dataMail));
            $votingProcess->voters_voting_processes->each(function ($reg, $key) {
                $GLOBALS["email"] = $reg->email;
                $GLOBALS["sendFirst"] = $key;
                //send email to user
                Mail::send('emails.invitation_to_vote', $GLOBALS['paramsMail'], function ($message) {
                    $message->subject('Invitación proceso de votación');
                    if ($GLOBALS["sendFirst"] == 0) {
                        $message->cc(['franciscozambrano@outlook.es', 'danielburbano19@gmail.com']);                        
                    }
                    $message->to($GLOBALS["email"]);
                });
            });

            $votingProcess->sendNotification = 1;
            $votingProcess->save();
        }
    }
}
