<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Import_type extends Model
{
    use SoftDeletes;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'import_types';

    //one-to-many relationships
    public function user_importer()
    {
        return $this->hasMany('App\User_importer','import_type');
    }
}
