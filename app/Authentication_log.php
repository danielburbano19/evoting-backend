<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Authentication_log extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'authentication_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'voter_id', 'voting_process_id', 'deleted_at'
    ];

    //one-to-one relationshipssss
    public function voting_process(){
        return $this->hasOne('App\Voting_process', 'voting_process_id');
    }   
}
