<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate_voting_process extends Model
{
    use SoftDeletes;
    
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'candidates_voting_processes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'candidate_id', 'voting_process_id'
    ];
}
