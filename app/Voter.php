<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'deleted_at'
    ];

    //many-to-many relationships
    public function voters_voting_processes(){
        return $this->belongsToMany('App\Voting_process', 'voters_voting_processes');         
    }

    //many-to-many relationships
    public function voters_voting_processes_enabled()
    {
        return $this->belongsToMany('App\Voting_process','voters_voting_processes')->whereIn('status', [0,1,4]);
    }    
}
