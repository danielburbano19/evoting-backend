<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter_request extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voters_request';


    //one-to-many (inverse) relationships 
    public function voting_process(){        
        return $this->hasOne('App\Voting_process');
    }    
}
