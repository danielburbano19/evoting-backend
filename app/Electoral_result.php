<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Electoral_result extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'electoral_results';

    //one-to-many (inverse) relationships
    public function candidate()
    {
        return $this->belongsTo('App\Candidate');
    }

    //one-to-many (inverse) relationships
    public function voting_process()
    {
        return $this->belongsTo('App\Voting_process');
    }

    public static function getElectoralResults($id)
    {
        $results = DB::table('electoral_results')
            ->join('candidates', 'electoral_results.candidate_id', '=', 'candidates.id')
            ->select(DB::raw('candidates.first_name, candidates.last_name, electoral_results.votes_count'))
            ->where('voting_process_id', $id)
            ->whereNull('electoral_results.deleted_at')
            ->groupBy('candidate_id')
            ->groupBy('candidates.first_name')
            ->groupBy('candidates.last_name')
            ->groupBy('electoral_results.votes_count')
            ->orderBy('electoral_results.votes_count', 'desc')
            ->get();

        return $results;
    }

    public static function getWinner($id)
    {
        //get max votes count
        $max_votes = DB::table('electoral_results')
            ->where('voting_process_id', $id)
            ->max('votes_count');

        //get winner
        $winner = DB::table('electoral_results')
            ->join('candidates', 'electoral_results.candidate_id', '=', 'candidates.id')
            ->select('candidates.first_name', 'candidates.last_name', 'votes_count')
            ->where('voting_process_id', $id)
            ->where('votes_count', "=", $max_votes)
            ->get();

        return $winner;
    }

    public static function getTotalVotes($id)
    {
        //get total votes count by voting process
        $totalVotes = DB::table('electoral_results')
            ->where('voting_process_id', $id)
            ->sum('votes_count');
        return $totalVotes;
    }

    public static function getCandidatesNumber($id)
    {
        //get total candidatesby voting process
        $candidatesNumber = DB::table('electoral_results')
            ->where('voting_process_id', $id)
            ->count('id');
        return $candidatesNumber;
    }

    public static function store($votingProcessId)
    {

        //delete if exist electoral results to update latest votes
        $electoralResults = Electoral_result::where('voting_process_id', $votingProcessId)->get();
        if (!$electoralResults->isEmpty()) {
            Electoral_result::where('voting_process_id', $votingProcessId)->each(function ($reg) {
                $reg->delete();
            });
        }

        //get count results
        $votesByCandidate = DB::table('candidates_voting_processes')
            ->select(DB::raw('candidates_voting_processes.voting_process_id, candidates_voting_processes.candidate_id, count(electoral_votes.candidate_id) as votes_count'))
            ->leftJoin('electoral_votes', 'electoral_votes.candidate_id', '=', 'candidates_voting_processes.candidate_id')
            ->where('candidates_voting_processes.voting_process_id', $votingProcessId)
            ->groupBy('candidates_voting_processes.voting_process_id')
            ->groupBy('candidates_voting_processes.candidate_id')
            ->orderBy('votes_count', 'desc')
            ->get();


        //save results in electoral_results
        $votesByCandidate->each(function ($reg) {
            $electoralResults = new Electoral_result();
            $electoralResults->voting_process_id = $reg->voting_process_id;
            $electoralResults->candidate_id = $reg->candidate_id;
            $electoralResults->votes_count = $reg->votes_count;
            $electoralResults->save();
        });
    }
}
