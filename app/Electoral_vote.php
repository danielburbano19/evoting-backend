<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Electoral_vote extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'electoral_votes';

    //one-to-many (inverse) relationships
    public function candidate()
    {
        return $this->belongsTo('App\Candidate');
    }    

    //one-to-many (inverse) relationships
    public function voting_process()
    {
        return $this->belongsTo('App\Voting_process');
    }    
}

