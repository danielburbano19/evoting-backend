<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';

    //one-to-many relationships
    public function users()
    {
        return $this->hasMany('App\User', 'role_id');
    }

    //many-to-many relationships
    public function modules()
    {
        return $this->belongsToMany('App\Module', 'modules_roles');
    }
}
