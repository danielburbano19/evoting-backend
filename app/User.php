<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use SoftDeletes;
    use Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [       
        'identification',
        'first_name',
        'last_name',
        'birthdate',
        'gender',
        'phone',
        'email',
        'address',
        'role',
        'user_name',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];    

    //one-to-many relationships
    public function user_importers(){
        return $this->hasMany('App\User_importer');
    }
        
    //one-to-many relationships
    public function voting_processes()
    {
        return $this->hasMany('App\Voting_process')->withTimestamps();
    }

    //one-to-many relationships
    public function user_sessions(){
        return $this->hasMany('App\User_session');
    }

    //one-to-many (inverse) relationships 
    public function role(){        
        return $this->belongsTo('App\Role');
    }    
}
