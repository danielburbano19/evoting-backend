<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voting_process extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voting_processes';

    //many-to-many relationships
    public function voters_voting_processes()
    {
        return $this->belongsToMany('App\Voter','voters_voting_processes');
    }    

    //many-to-many relationships
    public function candidates_voting_processes()
    {
        return $this->belongsToMany('App\Candidate', 'candidates_voting_processes' );
    }
   
    //one-to-many relationships inverse
    public function authenticate_logs_voting_processes()
    {
        return $this->belongsToMany('App\Authentication_logs');
    }

    //one-to-one inverse relationship
    public function user()
    {
        return $this->belongsTo('App\User')->withTimestamps();
    }    

    //one-to-many relationships
    public function importers_voting_processes()
    {
        return $this->hasMany('App\User_importer');
    }

    //one-to-many relationships
    public function voters_request()
    {
        return $this->hasMany('App\Voter_request');
    }

    //one-to-many relationships
    public function candidate_request()
    {
        return $this->hasMany('App\Voter_request');
    }

     //one-to-many relationships
     public function electoral_votes()
     {
        return $this->hasMany('App\Electoral_vote','voting_process_id');
     }   
}
