<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voter_session extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'voter_sessions';

    //one-to-many inverse relationships
    public function voter()
    {
        return $this->belongsTo('App\Voter');
    }
}
