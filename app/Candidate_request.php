<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Candidate_request extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'candidates_request';

    //one-to-many (inverse) relationships 
    public function voting_process(){        
        return $this->hasOne('App\Voting_process');
    }  
    
    //one-to-many (inverse) relationships 
    public function candidates(){        
        return $this->hasOne('App\Candidate');
    }  
}
