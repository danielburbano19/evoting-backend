<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Grant_permission extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grants_permissions';

    //many-to-many relationships
    public function modules()
    {
        return $this->belongsToMany('App\Module', 'grants_permissions_modules', 'grant_permission_id', 'id');
    }
    
    
}
