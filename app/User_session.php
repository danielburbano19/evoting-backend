<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User_session extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_sessions';

    //one-to-many inverse relationships
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
