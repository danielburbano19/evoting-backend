<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modules';

    //many-to-many relationships
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'modules_roles');
    }

    //many-to-many relationships
    public function grants_permissions()
    {
        return $this->belongsToMany('App\Grant_permission', 'grants_permissions_modules');
    }


}
