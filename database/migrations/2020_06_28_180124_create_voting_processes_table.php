<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotingProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voting_processes', function (Blueprint $table) {
            $table->id();
            $table->string('name', 256);
            $table->text('description');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->char('status', 1)->default('0');
            $table->string('image', 200)->nullable();            
            $table->foreignId('user_id');
            $table->char('sendNotification', 1)->default('0');
            $table->timestamps();  
            $table->softDeletes();          

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voting_processes');
    }
}
