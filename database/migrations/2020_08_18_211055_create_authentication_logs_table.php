<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthenticationLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authentication_logs', function (Blueprint $table) {
            $table->id();
            $table->string('voter_identification', 20)->nullable();
            $table->foreignId('voting_process_id');
            $table->string('score')->nullable();
            $table->char('status')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('voting_process_id')->references('id')->on('voting_processes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authentication_logs');
    }
}
