<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVotersRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters_request', function (Blueprint $table) {
            $table->id()->start;
            $table->string('identification', 20);
            $table->string('first_name', 256);
            $table->string('last_name', 256);            
            $table->string('email', 100);
            $table->longText('comments')->nullable();
            $table->foreignId('voting_process_id');
            $table->char('status')->default('0');
            $table->longText('reply')->nullable();
            $table->json('custom_data')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('voting_process_id')->references('id')->on('voting_processes');            
        });

        DB::statement('ALTER TABLE voters_request AUTO_INCREMENT = 1000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voters_request');
    }
}
