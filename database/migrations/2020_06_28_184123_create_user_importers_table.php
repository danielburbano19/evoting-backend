<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserImportersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('user_importers', function (Blueprint $table) {
            $table->id();
            //$table->foreignId('import_type_id');
            $table->string('file_name');
            $table->dateTime('upload_date');
            $table->foreignId('user_id');
            $table->foreignId('voting_process_id');
            $table->timestamps();
            $table->softDeletes();

            //$table->foreign('import_type_id')->references('id')->on('import_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('voting_process_id')->references('id')->on('voting_processes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_importers');
    }
}
