<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVotersVotingProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voters_voting_processes', function (Blueprint $table) {
            $table->id()->start;
            $table->foreignId('voter_id');
            $table->foreignId('voting_process_id');
            $table->char('registred_vote')->default('0');
            $table->dateTime('vote_date')->nullable();
            $table->char('authenticated')->default('0');
            $table->timestamps();
            $table->softDeletes();
             
            $table->foreign('voter_id')->references('id')->on('voters');
            $table->foreign('voting_process_id')->references('id')->on('voting_processes');
        });

        DB::statement('ALTER TABLE voters_voting_processes AUTO_INCREMENT = 1000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voters_voting_processes');
    }
}
