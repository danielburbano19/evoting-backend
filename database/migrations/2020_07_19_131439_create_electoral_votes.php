<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElectoralVotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('electoral_votes', function (Blueprint $table) {
            $table->id();
            $table->foreignId('voting_process_id');
            $table->foreignId('candidate_id');
            $table->string('ip_address', 50);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('voting_process_id')->references('id')->on('voting_processes');
            $table->foreign('candidate_id')->references('id')->on('candidates');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('electoral_votes');
    }
}
