<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('identification', 20);
            $table->unique(['identification', 'deleted_at']);
            $table->string('first_name', 256);
            $table->string('last_name', 256);
            $table->date('birthdate');
            $table->char('gender', 1);
            $table->string('email', 100);
            $table->json('custom_data')->nullable();
            $table->string('user_name', 100);
            $table->string('password', 256);                    
            $table->foreignId('role_id');
            $table->timestamps();
            $table->softDeletes();            

            $table->foreign('role_id')->references('id')->on('roles');           
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
