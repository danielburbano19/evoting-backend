<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates_request', function (Blueprint $table) {
            $table->id()->start;
            $table->foreignId('candidate_id');         
            $table->foreignId('voting_process_id');
            $table->longText('comments')->nullable();
            $table->char('status')->default('0');
            $table->longText('reply')->nullable();
            $table->json('custom_data')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('voting_process_id')->references('id')->on('voting_processes');
            $table->foreign('candidate_id')->references('id')->on('candidates');
        });
        DB::statement('ALTER TABLE candidates_request AUTO_INCREMENT = 1000;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_request');
    }
}
