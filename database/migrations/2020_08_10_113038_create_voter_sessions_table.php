<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoterSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voter_sessions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('voter_id');
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('ip_address', 50);
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('voter_id')->references('id')->on('voters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voter_sessions');
    }
}
