<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modulesArray = array();

        //User module----------------------------------------------

        $module['name'] = "User rosource";
        $module['endpoint'] = "/api/user";
        array_push($modulesArray, $module);

        $module['name'] = "User login";
        $module['endpoint'] = "/api/user/login";
        array_push($modulesArray, $module);

        $module['name'] = "User get image";
        $module['endpoint'] = "/api/user/image/";
        array_push($modulesArray, $module);

        //Role module----------------------------------------------

        $module['name'] = "Role rosource";
        $module['endpoint'] = "/api/role";
        array_push($modulesArray, $module);

        //Voter request module----------------------------------------------

        $module['name'] = "Voter request rosource";
        $module['endpoint'] = "api/voter_request";
        array_push($modulesArray, $module);

        foreach ($modulesArray as $module) {
            DB::table('modules')->insert([
                'name' => $module['name'],
                'endpoint' => $module['endpoint'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
