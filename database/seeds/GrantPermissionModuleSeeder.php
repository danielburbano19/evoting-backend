<?php

use App\Grant_permission;
use App\Module;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrantPermissionModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = Module::all();
        $grantsPermissions = Grant_permission::all();

        foreach ($grantsPermissions as $grantPermission) {
            foreach ($modules as $module) {
                DB::table('grants_permissions_modules')->insert([
                    'module_id' => $module->id,
                    'grant_permission_id' => $grantPermission->id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
            }            
        }
    }
}
