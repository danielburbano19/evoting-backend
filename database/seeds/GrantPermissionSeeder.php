<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrantPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grantsArray = ['C','R','U','D'];

        foreach ($grantsArray as $grant) {
            DB::table('grants_permissions')->insert([
                'grant_type' => $grant,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
