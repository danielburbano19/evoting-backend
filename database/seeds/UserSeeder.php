<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        DB::table('users')->insert([
            'identification' => '1085933225',
            'first_name' => 'Daniel Esteban',
            'last_name' => 'Burbano Salas',
            'gender' => 'M',
            'email' => 'danielburbano19@gmail.com',            
            'birthdate' => '1998-12-24',
            'user_name' => 'daniel',
            'password' => hash('sha256', '123'),   
            'role_id' => '1',   
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),         
        ]);
    }
}
