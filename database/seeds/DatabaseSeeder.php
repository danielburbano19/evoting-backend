<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(ModuleSeeder::class);
        $this->call(GrantPermissionSeeder::class); 
        $this->call(GrantPermissionModuleSeeder::class);        
        $this->call(GrantPermissionRoleSeeder::class);        
        $this->call(UserSeeder::class);
        //$this->call(VotingProcessSeeder::class);
    }
}
