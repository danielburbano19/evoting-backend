<?php

use App\Http\Controllers\VotingProcessController;
use App\Voting_process;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VotingProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //{"name":" ","description":"","start_date":"2020-09-08 19:29:00","end_date":"","user_id": "1"}
        DB::table('voting_processes')->insert([
            'name' => 'Proceso de votación para representante estudiantil',
            'description' => 'Proceso de votación para representante estudiantil 2020',
            'start_date' => '2020-09-08 19:29:00',
            'end_date' => '2020-09-09 23:36:00',
            'user_id' => '1',                           
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),         
        ]);

        VotingProcessController::setJobs();
    }
}
