const video = document.getElementById('video')
let processId = document.getElementById('voting_process_id').innerHTML;
const url = `http://localhost/evoting/public/api/voting_process_voters/labels/${processId}`
var labels;
let flagStoreAuthLogs = false;
let flagScore = false;
let matchResult;
let score;
fetch(url)
    .then(response => response.json())
    .then(data => {
        labels = data.voters_labels
            //console.log(labels)
    })
    .catch(error => {
        //save authentication logs
        if (!flagStoreAuthLogs) {
            storeAuthenticationLogs(processId, "", 0);
        }
        //console.log(error)
        window.location.href = "http://localhost:4200/process/access/error/1"
            //error id del proceso de votación no fue encontrado
    })

Promise.all([
    faceapi.nets.tinyFaceDetector.loadFromUri('http://localhost/evoting/public/models/'),
    faceapi.nets.faceLandmark68Net.loadFromUri('http://localhost/evoting/public/models/'),
    faceapi.nets.faceRecognitionNet.loadFromUri('http://localhost/evoting/public/models/'),
    faceapi.nets.faceExpressionNet.loadFromUri('http://localhost/evoting/public/models/'),
    faceapi.nets.ssdMobilenetv1.loadFromUri('http://localhost/evoting/public/models/')
]).then(startVideo)

function startVideo() {
    navigator.getUserMedia({
            video: {}
        },
        stream => video.srcObject = stream,
        err => console.error(err)
    )
}

video.addEventListener('play', () => {

    const canvas = faceapi.createCanvasFromMedia(video)
    let attemps = 0;
    let timeout = 0;
    let notification = false;
    document.body.append(canvas)
    const displaySize = {
        width: video.width,
        height: video.height
    }

    faceapi.matchDimensions(canvas, displaySize)
    setInterval(async() => {
        timeout++
        if (timeout > 20) {
            //save authentication logs
            if (!flagStoreAuthLogs) {
                storeAuthenticationLogs(processId, "", 0);
            }
            if (!notification) {
                sendNotification();
                notification = true;
            }
            window.location.href = `http://localhost:4200/process/access/error/2`;
            //error no se pudo reconocer al votante
        }
        const labeledFaceDescriptors = await loadLabeledImages()
        const faceMatcher = new faceapi.FaceMatcher(labeledFaceDescriptors, 0.6)

        const detections = await faceapi.detectAllFaces(video).withFaceLandmarks().withFaceDescriptors()
        const resizedDetections = faceapi.resizeResults(detections, displaySize)

        canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
        const results = resizedDetections.map(d => faceMatcher.findBestMatch(d.descriptor))
        results.forEach((result, i) => {
            const box = resizedDetections[i].detection.box
            const drawBox = new faceapi.draw.DrawBox(box, {
                label: result.toString()
            })
            drawBox.draw(canvas)
            matchResult = result.toString().split([' '])
            score = matchResult[1].substring(1, matchResult[1].length - 1)
                //score is success when this is closer to zero minimum to be recognized 0.4 and lower values
            if (result.toString().includes('unknown') || score > 0.52) {
                attemps++;
                //console.log('No se ha reconocido')
                if (attemps > 10) {
                    //save authentication logs
                    if (!flagStoreAuthLogs) {
                        storeAuthenticationLogs(processId, "", 0);
                    }
                    if (!notification) {
                        sendNotification();
                        notification = true;
                    }
                    window.location.href = `http://localhost:4200/process/access/error/2`;
                    //error no se pudo reconocer al votante
                }
            } else {
                if (!flagScore) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Reconocimiento exitoso',
                        showConfirmButton: false,
                        allowOutsideClick: false
                    })
                    flagScore = true
                    let finalScore = score
                        //console.log('Autenticación exitosa')
                    let processId = document.getElementById('voting_process_id').innerHTML;
                    identification = matchResult[0]
                        //console.log(identification)
                        //console.log(processId)

                    let urlencoded = new URLSearchParams();
                    urlencoded.append("json", `{\"voting_process_id\":\"${processId}\",\"voter_identification\":\"${identification}\"}`);

                    let requestOptions = {
                        method: 'POST',
                        body: urlencoded,
                        redirect: 'follow'
                    };

                    fetch("http://localhost/evoting/public/api/voter/authenticate", requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            if (data.code == 200) {
                                if (!flagStoreAuthLogs) {
                                    //save photo
                                    savePhotoAuth(canvas)
                                    video.play()
                                    setTimeout(() => {

                                        //save authentication logs                            
                                        storeAuthenticationLogs(processId, identification, 1, finalScore);

                                        //save photo 
                                        savePhotoAuth(canvas)

                                        //redirect
                                        window.location.href = `http://localhost:4200/process/${processId}/${identification}/vote`;
                                    }, 6000);
                                }
                            } else {
                                //save authentication logs                    
                                if (!flagStoreAuthLogs) {
                                    storeAuthenticationLogs(processId, "", 0);
                                }
                                if (!notification) {
                                    sendNotification();
                                    notification = true;
                                }
                                window.location.href = `http://localhost:4200/process/access/error/3`;
                                //error al autenticar al votante por favor intentar nuevamente
                            }
                        })
                        .catch(error => {
                            if (!flagStoreAuthLogs) {
                                storeAuthenticationLogs(processId, "", 0);
                            }
                            if (!notification) {
                                sendNotification();
                                notification = true;
                            }
                            window.location.href = `http://localhost:4200/process/access/error/3`;
                            //error al autenticar al votante por favor intentar nuevamente
                        });
                }
            }
        })
    }, 1000)
})

function storeAuthenticationLogs(votingProcessId, voterId, status, score = "") {
    flagStoreAuthLogs = true;
    let urlencoded = new URLSearchParams();
    urlencoded.append("json", `{\"voting_process_id\":\"${votingProcessId}\",\"voter_identification\":\"${voterId}\",\"score\":\"${score}\",\"status\":\"${status}\"}`);
    let requestOptions = {
        method: 'POST',
        body: urlencoded,
        redirect: 'follow'
    };

    fetch("http://localhost/evoting/public/api/authentication_logs", requestOptions)
        .then(response => response.json())
        .then(data => {
            if (data.code != 200) {
                window.location.href = `http://localhost:4200/process/access/error/3`;
                //error al autenticar al votante por favor intentar nuevamente
            }
        })
        .catch(error => {
            window.location.href = `http://localhost:4200/process/access/error/3`;
            //error al autenticar al votante por favor intentar nuevamente
        });
}


function loadLabeledImages() {
    //const labels = ['1085933225', '12345678']  
    return Promise.all(
        labels.map(async label => {
            const descriptions = []
            const imgUrl = `http://localhost/evoting/storage/app/voters/${label}.jpg`
            const img = await faceapi.fetchImage(imgUrl)
            const detections = await faceapi.detectSingleFace(img).withFaceLandmarks().withFaceDescriptor()
            descriptions.push(detections.descriptor)
            return new faceapi.LabeledFaceDescriptors(label, descriptions)
        })
    )
}

function savePhotoAuth(canvas) {
    video.pause();
    //get context and draw on it
    let contexto = canvas.getContext("2d");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    contexto.drawImage(video, 0, 0, canvas.width, canvas.height);

    //image in base 64
    let photo = canvas.toDataURL();

    let dataImg = {
        "voting_process": processId,
        "voter_id": identification,
        "img_64": encodeURIComponent(photo)
    }
    dataImg = JSON.stringify(dataImg)
        //send request with imgage to save it
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost/evoting/public/api/voter/save_auth_img", true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send((dataImg));
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {

        }
    }
}

function sendNotification() {

    var requestOptions = {
        method: 'GET',
        redirect: 'follow'
    };

    fetch("https://api.ipify.org?format", requestOptions)
        .then(response => response.text())
        .then(result => {
            var urlencoded = new URLSearchParams();
            urlencoded.append("json", `{\"voting_process_id\":\"${processId}\",\"ip_address\":\"${result}\"}`);

            var requestOptions = {
                method: 'POST',
                body: urlencoded,
                redirect: 'follow'
            };

            fetch("http://localhost/evoting/public/api/send_notification", requestOptions)
                .then(response => response.json())
                .then(data => {
                    if (data.code != 200) {
                        window.location.href = `http://localhost:4200/process/access/error/3`;
                        //error al autenticar al votante por favor intentar nuevamente
                    }
                })
                .catch(error => {
                    window.location.href = `http://localhost:4200/process/access/error/3`;
                    //error al autenticar al votante por favor intentar nuevamente
                });
        })
        .catch(error => console.log('error', error));

}