<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
            'endpoint' => env('AWS_ENDPOINT'),
        ],

        'users' => [
            'driver' => 'local',
            'root' => storage_path('app/users'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voters' => [
            'driver' => 'local',
            'root' => storage_path('app/voters'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'candidates' => [
            'driver' => 'local',
            'root' => storage_path('app/candidates'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',            
        ],

        'voting_processes' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_processes'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voting_processes_results' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_processes_results'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voting_certificates' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_certificates'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voting_process_candidates' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_process_candidates'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voting_process_voters' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_process_voters'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        'voting_process_voters_auth' => [
            'driver' => 'local',
            'root' => storage_path('app/voting_process_voters_auth'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Symbolic Links
    |--------------------------------------------------------------------------
    |
    | Here you may configure the symbolic links that will be created when the
    | `storage:link` Artisan command is executed. The array keys should be
    | the locations of the links and the values should be their targets.
    |
    */

    'links' => [
        public_path('storage') => storage_path('app/public'),
        public_path('storage/candidates') => storage_path('app/candidates'),
        public_path('storage/voting_certificates') => storage_path('app/voting_certificates'),        
        public_path('storage/voting_process_candidates') => storage_path('app/voting_process_candidates'),       
        public_path('storage/voting_processes_results') => storage_path('app/voting_processes_results'),    
        public_path('storage/voting_process_voters') => storage_path('app/voting_process_voters'),  
        public_path('storage/voting_process_voters_auth') => storage_path('app/voting_process_voters_auth'),                              
    ],

];
