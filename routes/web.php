<?php

use App\Http\Middleware\ApiAuthMiddleware;
use App\Http\Middleware\ApiLog;
use App\Jobs\StarFinishVotingProcesses;
use App\Jobs\StartFinishVotingProcesses;
use App\Jobs\Test;
use App\Voting_process;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use PHPUnit\Framework\MockObject\Api;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testorm', 'TestOrm@testOrm')->middleware(ApiLog::class);
//Route::resource('/api/user_info','UserInfoController')->middleware(ApiAuthMiddleware::class);
//Route::resource('/api/import_type','ImportTypeController');

//Route user
Route::resource('/api/user', 'UserController')->middleware(ApiLog::class);
Route::post('/api/user/login', 'UserController@login')->middleware(ApiLog::class);
Route::get('/api/user/image/{identification}', 'UserController@getImage')->middleware(ApiLog::class);

//Route voting process
Route::resource('/api/voting_process', 'VotingProcessController')->middleware(ApiLog::class);
Route::post('/api/voting_process/change_status', 'VotingProcessController@changeStatus')->middleware(ApiLog::class);
Route::get('/api/voting_process_candidates/{id}', 'VotingProcessController@candidates')->middleware(ApiLog::class);
Route::get('/api/voting_process_voters/{id}', 'VotingProcessController@voters')->middleware(ApiLog::class);
Route::get('/api/voting_process_voters/labels/{id}', 'VotingProcessController@votersLabels')->middleware(ApiLog::class);
Route::get('/api/voting_process/image/{id}', 'VotingProcessController@getImage')->middleware(ApiLog::class);
Route::get('/api/voting_process/electoral_result/{id}', 'VotingProcessController@getResultsByProcess')->middleware(ApiLog::class);
Route::get('/api/voting_process/winner/{id}', 'VotingProcessController@getWinnerByProcess')->middleware(ApiLog::class);
Route::get('/api/voting_process/send_invitation_email/{id}', 'VotingProcessController@sendInviteEmailsByproccess')->middleware(ApiLog::class);

//Route user importer
Route::resource('/api/user_importer', 'UserImporterController')->middleware(ApiLog::class);

//Route user session
Route::resource('/api/user_session', 'UserSessionController')->middleware(ApiLog::class);

//Route role
Route::resource('/api/role', 'RoleController')->middleware(ApiLog::class);

//Route candidate
Route::resource('/api/candidate', 'CandidateController')->middleware(ApiLog::class);
Route::get('/api/candidate/image/{identification}', 'CandidateController@getImage')->middleware(ApiLog::class);

//Route voter
Route::resource('/api/voter', 'VoterController')->middleware(ApiLog::class);
Route::get('/api/voter/voter_enabled/{identification}', 'VoterController@voterEnabled')->middleware(ApiLog::class)->middleware(ApiLog::class);
Route::post('/api/voter/upload_image/', 'VoterController@uploadImage')->middleware(ApiLog::class);
Route::get('/api/voter/image/{identification}', 'VoterController@getImage')->middleware(ApiLog::class);
Route::post('/api/voter/registerd_vote/', 'VoterController@voterRegisterdVote')->middleware(ApiLog::class);
Route::post('/api/voter/authenticate/', 'VoterController@authenticateVoter')->middleware(ApiLog::class);
Route::post('/api/voter/get_token', 'VoterController@checkAuthenticatedVoter')->middleware(ApiLog::class);
Route::post('/api/voter/save_auth_img', 'VoterController@saveAuthImg')->middleware(ApiLog::class);


//Route electoral proposals
Route::resource('/api/electoral_proposals', 'ElectoralProposalsController')->middleware(ApiLog::class);

//Route voter request
Route::resource('/api/voter_request', 'VoterRequestController')->middleware(ApiLog::class);
Route::get('/api/voter_request/get_by_identification/{identification}', 'VoterRequestController@getVoterRequest')->middleware(ApiLog::class);
Route::post('/api/voter_request/reply', 'VoterRequestController@reply')->middleware(ApiLog::class);


//Route electoral votes
Route::resource('/api/electoral_vote', 'ElectoralVoteController')->middleware(ApiLog::class);

//Route authentication
Route::get('/authentication/{processId}', 'AuthenticateController@index')->middleware(ApiLog::class);
Route::post('/api/send_notification', 'AuthenticateController@sendNotification')->middleware(ApiLog::class);

//Route authentication logs
Route::resource('/api/authentication_logs', 'AuthenticationLogController')->middleware(ApiLog::class);

//Route voter request
Route::resource('/api/candidate_request', 'CandidateRequestController')->middleware(ApiLog::class);
Route::get('/api/candidate_request/get_by_identification/{identification}', 'CandidateRequestController@getCandidateRequest')->middleware(ApiLog::class);
Route::post('/api/candidate_request/reply', 'CandidateRequestController@reply')->middleware(ApiLog::class);

//Route logs
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware(ApiLog::class);